 
<?php include('../db.php'); ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
  <meta name="author" content="GeeksLabs">
  <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
  <link rel="shortcut icon" href="../img/favicon.png">

  <title>Dashboard :: Picadas Macanudas</title>
  <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

  <!-- Bootstrap CSS -->
  
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <!-- bootstrap theme -->
  <link href="../css/bootstrap-theme.css" rel="stylesheet">
  <!--external css-->
  <!-- font icon -->
  <link href="../css/elegant-icons-style.css" rel="stylesheet" />
  <link href="../css/font-awesome.min.css" rel="stylesheet" />
  <!-- full calendar css-->
  <link href="../assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
  <link href="../assets/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" />
  <!-- easy pie chart-->
  <link href="../assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen" />
  <!-- owl carousel -->
  <link rel="stylesheet" href="../css/owl.carousel.css" type="text/css">
  <link href="../css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
  <!-- Custom styles -->
  <link rel="stylesheet" href="../css/fullcalendar.css">
  <link href="../css/widgets.css" rel="stylesheet">
  <link href="../css/bootstrap.min.css" rel="stylesheet">

  <link href="../css/style.css" rel="stylesheet">
  <link href="../css/style-responsive.css" rel="stylesheet" />
  <link href="../css/xcharts.min.css" rel=" stylesheet">
  <link href="../css/jquery-ui-1.10.4.min.css" rel="stylesheet">
  <!-- =======================================================
    Theme Name: NiceAdmin
    Theme URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->

</head>

<body>
  <!-- container section start -->
  <section id="container" class="">


    <header class="header dark-bg">
      <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
      </div>

      <!--logo start-->
      <a href="index.html" class="logo">Pachi <span class="lite">Man</span></a>
      <!--logo end-->
      <!-- set up the modal to start hidden and fade in and out -->

      
      </div>
    </header>
      <header style="background: transparent; margin:3% 20% 2% 35%; border-block-end: inherit; "id="header" class="header">
              <?php 
                  if(isset($_SESSION['message'])){
                  if($_SESSION['message'] != false) { ?>
                      <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
                          <?php echo $_SESSION['message'] ;?>
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      </div>
                  <?php  
                  }
              }
                  $_SESSION['message_type'] = false;
                  $_SESSION['message'] = false;
                  
              ?>
          </header>
    <!--header end-->

    <!--sidebar start-->
    <aside>
      <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
          <li class="active">
            <a class="" href="index.html">
                          <i class="icon_house_alt"></i>
                          <span>Dashboard</span>
                      </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_document_alt"></i>
                          <span>Pedidos</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="../views/pedidos_conenvio.php">con envio</a></li>
              <li><a class="" href="../views/pedidos_conretiro.php">con retiro</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_desktop"></i>
                          <span>Preparar</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="../views/picadas_lunes.php">Lunes</a></li>
              <li><a class="" href="../views/picadas_martes.php">Martes</a></li>
              <li><a class="" href="../views/picadas_miercoles.php">Miercoles</a></li>
              <li><a class="" href="../views/picadas_jueves.php">Jueves</a></li>
              <li><a class="" href="../views/picadas_viernes.php">viernes</a></li>
              <li><a class="" href="../views/picadas_sabados.php">Sábados</a></li>
              <li><a class="" href="../views/pedidos_futuro.php">Futuro</a></li>



            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_desktop"></i>
                          <span>Fletes</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="../views/tabla_localidades.php">Localidades</a></li>
              <li><a class="" href="../views/tabla_fletes.php">Planilla para flete</a></li>
            </ul>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_desktop"></i>
                          <span>Stock</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="../views/stock_picadas.php">Picadas</a></li>
              <li><a class="" href="../views/stock_proveedores.php">Proveedores</a></li>
            </ul>
          </li>
         
          <li>
            <a class="" href="../views/picadas_entregadas.php">
                          <i class="icon_piechart"></i>
                          <span>Entregadas</span>

                      </a>

          </li>
          </li>
          <li class="sub-menu">
            <a href="javascript:;" class="">
                          <i class="icon_table"></i>
                          <span>Estadisticas</span>
                          <span class="menu-arrow arrow_carrot-right"></span>
                      </a>
            <ul class="sub">
              <li><a class="" href="../views/profit.php">Profit</a></li>
              <li><a class="" href="../views/satisfaccion_del_cliente.php">Satisfaccion del cliente</a></li>
            </ul>
            <ul class="sub">
              
            </ul>
          </li>
          <li>
            <a class="" href="../views/tickets_desarrollo.php">
                          <i class="icon_piechart"></i>
                          <span>Tickets</span>

                      </a>

          </li>

          

        </ul>
        <!-- sidebar menu end-->
      </div>
    </aside>
    <!--sidebar end-->
    

    <!--main content start-->
    <section id="main-content">
      <section class="wrapper">
        <!--overview start-->
        
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box blue-bg">
              <i class="fa fa-cloud-download"></i>
              <div class="count">6.674</div>
              <div class="title">Picadas para la semana</div>
            </div>
            <!--/.info-box-->
          </div>
          <!--/.col-->

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box brown-bg">
              <i class="fa fa-shopping-cart"></i>
              <div class="count">7.538</div>
              <div class="title">Picadas para Hoy</div>
            </div>
            <!--/.info-box-->
          </div>
          <!--/.col-->

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box dark-bg">
              <i class="fa fa-thumbs-o-up"></i>
              <div class="count">4.362</div>
              <div class="title">Que se entregan hoy</div>
            </div>
            <!--/.info-box-->
          </div>
          <!--/.col-->

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="info-box green-bg">
              <i class="fa fa-cubes"></i>
              <div class="count">1.426</div>
              <div class="title">Stock</div>
            </div>
            <!--/.info-box-->
          </div>
          <!--/.col-->

        </div>
        <!--/.row-->
        <div class="row">
          <div class="col-lg-12">
            <section class="panel">
              <header class="panel-heading">
                Picadas para LUNES
              </header>
            
       

              <table class="table table-striped table-advance table-hover">
                <tbody>
                  <tr>
                    <th>id</th>
                    <th>Fecha</th>
                    <th>Cliente</th>
                    <th>Picada</th>
                    <th>Agregado</th>
                    <th>Agregado Stock</th>
                    <th>Medio de Pago</th>
                    <th>Opciones</th>
                  </tr>

                  <?php

                                        
                    $query = "SELECT * FROM `general` WHERE `delivery_date` <= (DATE_ADD(CURDATE(), INTERVAL 6 DAY)) AND delivery_date >= CURDATE() AND WEEKDAY(`delivery_date`) = 0"  ;
                    $result = mysqli_query($conn, $query);                        
                    while($row = mysqli_fetch_assoc($result)) { 

                      $id = $row['id'];
                      $delivery_date = $row['delivery_date'];
                      $customer = $row['customer'];
                      $cel_phone = $row['cel_phone'];
                      $cnee = $row['cnee'];
                      $cnee_cel_phone = $row['cnee_cel_phone'];
                      $inscription = $row['inscription'];
                      $schedule_available = $row['schedule_available'];
                      $product = $row['product'];
                      $add1 = $row['add1'];
                      $add2 = $row['add2'];
                      $payment_mode = $row['payment_mode'];
                      $address = $row['address'];
                      $referencia = $row['referencia'];
                      $location = $row['location'];
                      $status = $row['status'];

                      
                      ?>

                  <tr>
                    <td><?php echo $id;?></td>
                    <td><?php echo $delivery_date;?></td>
                    <td><?php echo $customer;?></td>
                    <td><?php echo $product;?></td>
                    <td><?php echo $add1;?></td>
                    <td><?php echo $add2;?></td>
                    <td><?php echo $payment_mode;?></td>
                    <td>
                      <div class="btn-group">
                        <a class="btn btn-primary" title="Ver Detalle" href="#" type="button"  data-toggle="modal" data-target="#ver<?php echo $id;?>"><i class="fa fa-eye"></i></a>
                          
                          <!--Modal asigned CNTR-->
                          <div class="modal fade" id="ver<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                    <h4 style="text-align:center;"class="modal-title" id="scrollmodalLabel">Detalles de Picada para <strong><?php echo $customer;?></strong></h4>    
                                </div>
                                <div class="modal-body">
                                  <div class="card border border-secondary">
                                    <div class="card-body">
                                      <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-8" > 
                                          <h4 style="text-align:center;"> <strong> Nombre: </strong> <?php echo $customer .' ('.$cel_phone . ') ' ?> </h4>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-8" > 
                                          <h4 style="text-align:center;"> <strong> Receptor: </strong> <?php echo $cnee .' ('.$cnee_cel_phone . ') ' ?> </h4>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-8" > 
                                          <h4 style="text-align: center;"> <strong> Dedicatoria: </strong></h4>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-8" > 
                                          <h4 style="text-align: center;"><?php echo $inscription;?></h4>
                                        </div>
                                      </div> 
                                      <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-8" > 
                                          <h4 style="text-align: center;"> <strong> Fecha: </strong> <?php echo $delivery_date ; ?> </h4>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-8" > 
                                          <h4 style="text-align: center;"> <strong> Horario: </strong> <?php echo $schedule_available ; ?> </h4>
                                        </div>
                                      </div> 
                                      <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-8" > 
                                          <h4 style="text-align: center;"> <strong> Direccion: </strong> <?php echo $address . ' ( '. $referencia . ') - '. $location ;?> </h4>
                                        </div>
                                      </div> 
                                      <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-8" > 
                                          <h4 style="text-align: center;"><strong>Picada: </strong> <?php echo $product . ' ( '. $add1 . ')';?></h4>
                                        </div>
                                      </div> 
                                      <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-8" > 
                                          <h4 style="text-align: center;"><strong>Agregados:</strong> <?php echo $add2;?></h4>
                                        </div>
                                      </div> 
                                      <div class="row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-8" > 
                                          <h4 style="text-align: center;"><strong>Forma de Pago:</strong> <?php echo $payment_mode; ?></h4>
                                        </div>
                                      </div> 
                                    </div>
                                    <br>
                                    <div class="row">
                                      <div class="col-sm-4"></div>
                                      <div class="col-sm-4" style="text-align: center;">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">cerrar</button>
                                      </div>
                                      <div class="col-sm-4"></div>
                                    </div> 
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!--Final Modal View CNTR-->            
                        <a class="btn btn-success" title="Cambiar Status" href="#" type="button"  data-toggle="modal" data-target="#cambiar_status<?php echo $id;?>"><i class="fa fa-check"></i></a>
                        <!--Modal asigned CNTR-->
                        <div class="modal fade" id="cambiar_status<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                  <h4 style="text-align:center;"class="modal-title" id="scrollmodalLabel">Cambiar estado a Picada <strong><?php echo $id;?></strong></h4>    
                              </div>
                              <div class="modal-body">
                                <div class="card border border-secondary">
                                  <div class="card-body">
                                  <h3 style="text-align: center;"> <strong> Status: </strong></h3>
                                  <form action="../forms/cambia_status.php?dia=lunes" method="POST">
                                    <div class="row">
                                      <div class="col-sm-2"></div>
                                      <div class="col-sm-8">
                                        <select name="status[]" id="selectSm" class="form-control form-control">
                                          <option value="<?php echo $status; ?>"><?php echo $status; ?></option>     
                                              <?php
                                                  
                                                  $mysqli = new mysqli('localhost', 'root', '', 'macanudas'); 
              
                                                  $query_status = $mysqli -> query ("SELECT * FROM `status_pícadas`");
                                                          while ($status= mysqli_fetch_array($query_status)) {                                           
                                                              echo '<option value="'.$status['title'].'">'.$status['title'].'</option>';
                                                          }  
                                              ?>
                                        </select>  
                                      </div> 
                                    </div>
                                    <br>
                                    <div class="row">
                                      <div class="col-sm-4"></div>
                                      <div class="col-sm-4" style="text-align: center;">
                                        <button type="submit" name="confirmar" id="confirmar" class="btn btn-warning">Cambiar</button>
                                       
                                      </div>
                                      <div class="col-sm-4"></div>
                                    </div> 
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!--Final Modal View CNTR-->   

                        <a class="btn btn-danger" target="_blank" href="https://api.whatsapp.com/send?phone=54<?php echo $cel_phone;?>&text=Hola,%20te%20escribo%20de%20Picadas%20Macanudas"title="Contactar" href="#"><i class="fa fa-user"></i></a>
                      </div>
                    </td>
                  </tr>
                  
                </tbody>
                    <?php };?>
              </table>
            </section>
          </div>
        </div>


 <!-- container section end -->
  <!-- javascripts -->
  <script src="../js/jquery.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <!-- nicescroll -->
  <script src="../js/jquery.scrollTo.min.js"></script>
  <script src="../js/jquery.nicescroll.js" type="text/javascript"></script>
  <!--custome s../cript for all page-->
  <script src="../js/scripts.js"></script>
  


</body>

</html>