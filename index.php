<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-180172331-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-180172331-1');
    </script>

    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Picadas Macanudas :: sabores que compartimos</title>
    <meta name="description" content="Picadas en Mendoza. Acompañá tus buenos momentos con nuestras picadas.">
    <meta name="keywords" content="">
    <meta name="author" content="buildit" />



    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600,600i,700,700i|Satisfy|Comic+Neue:300,300i,400,400i,700,700i"
        rel="stylesheet">
    <link
        href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Piazzolla:ital,wght@0,300;0,800;0,900;1,500&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">

    <style>
    @font-face {
        font-family: "Nickainley";
        src: url("assets/font/Nickainley-Normal.otf");
    }
    </style>
    <!-- Vendor CSS Files -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
    <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">
    <!-- =======================================================
  * Template Name: Delicious - v2.1.0
  * Template URL: https://bootstrapmade.com/delicious-free-restaurant-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
    $(document).ready(function() {
        $("#myModal").modal('show');
    });
    </script>
</head>

<body>


    <!-- ======= Top Bar ======= -->
    <section id="topbar" class="d-none d-lg-flex align-items-center fixed-top topbar-transparent">
        <div class="container text-right">
            <i class="icofont-phone"></i> +54 9 2613 40-5502
            <i class="icofont-clock-time icofont-rotate-180"></i> Lun-Sáb: 10:00 AM - 20:00 PM
        </div>
    </section>

    <!--div id="myModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-lg ">
            <div class="modal-content border-warning mt-5 mb-5">
                <div class="modal-body">
                    <div class="section-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <h2>Novedades de<span> fin de año</span> </h2>
                            </div>
                            <div class="col-sm-1 align-self-center">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="owl-carousel events-carousel mb-3 ">
                    <div class="row event-item">
                        <div class="col-lg-6">
                            <img src="assets/img/espumante-pacto.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 pt-4 pt-lg-0 content">
                            <h2>Vino Espumante:</h2>
                            <p class="m-0">Pinot Noir brut nature</p>
                            <span class="badge badge-warning">Partida Limitada</span>
                            <br>
                            <br>
                            <h3 style="text-align: center;"><strong>Agragalo <br> a tus Pedidos de Picadas!</strong>
                            </h3>
                            <p class="text-warning" style="text-align: center; font-size: larger;">$ 500.00</p>
                            <br>
                        </div>
                    </div>
                    <div class="row event-item">
                        <div class="col-lg-6">
                            <img src="assets/img/box-macanuda.jpeg" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 pt-4 pt-lg-0 content">
                            <h2>Box de finde año:</h2>
                            <p>
                                - Espumante 🍾<br>
                                - Chorizo seco 250 gr 🍖<br>
                                - Horma de queso saborizado 🧀800 gr <br>
                                - Grisines caseros 🥖<br>
                                - Mix de chocolates 🍫-<br>
                            </p>
                            <h3 style="text-align: center;">$ <strong>3400,00</strong></h3>
                        </div>
                    </div>
                    <div class="row event-item">
                        <div class="col-lg-6">
                            <img src="assets/img/bolsa-macanuda.jpeg" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 pt-4 pt-lg-0 content">
                            <h2>Bolsa de finde año:</h2>
                            <p>
                                - Espumante 🍾<br>
                                - Chorizo seco 250 gr 🍖<br>
                                - Horma de queso saborizado 🧀800 gr <br>
                                - Grisines caseros 🥖<br>
                                - Mix de chocolates 🍫-<br>
                            </p>
                            <h3 style="text-align: center;">$ <strong>2600,00</strong></h3>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    </div-->

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top d-flex align-items-center header-transparent">
        <a href="index.php"><img style="width: 25%; margin-left:25%;" src="assets/img/MCND_Logo_1080_WHITE.png"
                alt="picadas mendoza"></a>
        <div class="container d-flex align-items-center">

            <div class="logo mr-auto">
                <h1 class="text-light"><a href="index.php"></a></h1>
                <!-- Uncomment below if you prefer to use an image logo -->
                <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
            </div>

            <nav class="nav-menu d-none d-lg-block">
                <ul>
                    <li class="active"><a href="index.php">Inicio</a></li>
                    <li><a href="#about">Nosotros</a></li>
                    <li><a href="#menu">Picadas</a></li>
                    <li><a href="#events">Agregados</a></li>
                    <li><a href="#gallery">Galeria</a></li>
                    <li><a href="#contact">Contacto</a></li>
                </ul>
            </nav><!-- .nav-menu -->

        </div>
    </header><!-- End Header -->

    <!-- ======= Hero Section ======= -->
    <section id="hero">
        <div class="hero-container">
            <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

                <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

                <div class="carousel-inner" role="listbox">

                    <!-- Slide 1 -->
                    <div class="carousel-item active" style="background: url(assets/img/slide/Macanudas-71.jpg);">
                        <div class="carousel-container">
                            <div class="carousel-content">
                                <img style="width: 60%;" src="assets/img/MCND_Logo_2160_WHITE.png" alt="">
                                <p class="animate__animated animate__fadeInUp">Somos mucho más de lo que
                                    comemos, pero
                                    podemos hacer que lo que comemos nos ayude a ser más de lo que somos.</p>
                                <div>
                                    <a href="control/forms/forms_envios.php"
                                        class="btn-menu animate__animated animate__fadeInUp scrollto">Pedido con
                                        Envío</a>
                                    <a href="control/forms/forms_retiros.php"
                                        class="btn-book animate__animated animate__fadeInUp scrollto">Pedido con
                                        Retiro</a>
                                    <a href="control/forms/forms_fin_de_año.php"
                                        class="btn-book animate__animated animate__fadeInUp scrollto">Enamorados</a>
                                    <!--a href="control/forms/forms_promociones.php" class="btn-book animate__animated animate__fadeInUp scrollto">Día de la Madre</a-->

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Slide 2 -->
                    <div class="carousel-item" style="background: url(assets/img/slide/Macanudas-9.jpg);">
                        <div class="carousel-container">
                            <div class="carousel-content">
                                <img style="width: 60%;" src="assets/img/MCND_Logo_2160_WHITE.png" alt="">
                                <p class="animate__animated animate__fadeInUp">El silencio es el sonido de una
                                    buena
                                    picada. </p>
                                <div>
                                    <a href="control/forms/forms_envios.php"
                                        class="btn-menu animate__animated animate__fadeInUp scrollto">Pedido con
                                        Envío</a>
                                    <a href="control/forms/forms_retiros.php"
                                        class="btn-book animate__animated animate__fadeInUp scrollto">Pedido con
                                        Retiro</a>
                                    <a href="control/forms/forms_fin_de_año.php"
                                        class="btn-book animate__animated animate__fadeInUp scrollto">Enamorados</a>
                                    <!--a href="control/forms/forms_promociones.php" class="btn-book animate__animated animate__fadeInUp scrollto">Día de la Madre</a-->

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Slide 3 -->
                    <div class="carousel-item" style="background: url(assets/img/slide/Macanudas-53.jpg);">
                        <!--div class="carousel-background"><img src="assets/img/slide/Macanudas-78.jpg" alt=""></div-->
                        <div class="carousel-container">
                            <div class="carousel-content">
                                <img style="width: 60%;" src="assets/img/MCND_Logo_2160_WHITE.png" alt="">
                                <p class="animate__animated animate__fadeInUp">No se puede comprar la felicidad
                                    pero
                                    puedes comerte una de nuestras picadas. Que es casi lo mismo.</p>
                                <div>
                                    <a href="control/forms/forms_envios.php"
                                        class="btn-menu animate__animated animate__fadeInUp scrollto">Pedido con
                                        Envío</a>
                                    <a href="control/forms/forms_retiros.php"
                                        class="btn-book animate__animated animate__fadeInUp scrollto">Pedido con
                                        Retiro</a>
                                    <a href="control/forms/forms_fin_de_año.php"
                                        class="btn-book animate__animated animate__fadeInUp scrollto">Enamorados</a>
                                    <!--a href="control/forms/forms_promociones.php" class="btn-book animate__animated animate__fadeInUp scrollto">Día de la Madre</a-->

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon icofont-simple-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon icofont-simple-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        </div>
    </section><!-- End Hero -->

    <main id="main">

        <!-- ======= About Section ======= -->
        <section id="about" class="about">
            <div class="container-fluid">

                <div class="row">

                    <div class="col-lg-5" style="padding: 0px;">
                        <img src="assets/img/Macanudas-85.jpg" class="img-fluid" alt="">
                    </div>

                    <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch">

                        <div class="content">
                            <h3 style="color: #ffb03b;"><strong>¿Quiénes Somos?</strong></h3>
                            <p>
                                Somos una familia Mercedina de origen, mendocina por adopción que ante el grito
                                de
                                nuestros amigos mendocinos de “cuando vayas a Mercedes tráeme salame!” decidimos
                                compartir estos tradicionales sabores del campo pampeano.</p>
                            </p>
                            <div class="text-center"><button type="submit" class="btn btn-outline-secondary"
                                    data-toggle="modal" data-target="#macanudas">¿Por qué macanudas?</button>
                            </div>
                            <br>

                            <h3 style="color: #ffb03b;"><strong>Mercedes</strong></h3>

                            <p class="font-italic">
                                Mercedes es una ciudad situada a 100 km al oeste de la Capital Federal, y es
                                sede de la
                                Fiesta Nacional del Salame Quintero.
                            </p>

                            <div class="text-center"><button type="submit" class="btn btn-outline-secondary"
                                    data-toggle="modal" data-target="#historia">un poco de Historia</button>
                            </div>

                            <div class="modal fade" id="historia" tabindex="-1" role="dialog"
                                aria-labelledby="scrollmodalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body" style="padding: 0%;">
                                            <br>
                                            <h3 style="text-align: center !important; color: #ffb03b;">Historia:
                                            </h3>
                                            <br>
                                            <img style="max-width: -webkit-fill-available;"
                                                src="assets/img/Mercedes.png" alt="mrecedes">
                                            <br>
                                            <br>
                                            <p style="margin: 0% 15% 5%; text-align: center;">Durante el siglo
                                                XIX y
                                                principio del XX, arribaron a la zona inmigrantes de la
                                                Península
                                                itálica e ibérica, una vez instalados en sus llamadas “quintas o
                                                chacras” introdujeron en estas tierras la artesanía culinaria
                                                del
                                                chacinado que ancestralmente fueron transmitidas de generación
                                                en
                                                generación y que por esas razones convergentes, de buena carne
                                                porcina,
                                                mezclada a la excelente carne bovina, sumando a las aguas, el
                                                clima y
                                                las especies, comenzó a desarrollarse un llamado “salame
                                                quintero” que
                                                tenía particularidades de color y sabor que se afincaron en
                                                nuestra
                                                zona, para trascender en el gusto de la gente hasta hacerse
                                                conocido el
                                                dicho que los mejores salames son los de la zona de Mercedes.
                                            </p>

                                            <p style="margin: 0% 15% 5%; text-align: center;"> Es así que el
                                                salame
                                                quintero se convirtió en el pretexto para que familias o amigos
                                                se
                                                reúnan en interminables tertulias, donde los chacinados y la
                                                charla
                                                sirven para pasar un grato momento.</p>


                                            <div class="text-center">
                                                <button type="button" class="btn btn-outline-secondary rounded-circle"
                                                    data-dismiss="modal">X</button>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="modal fade" id="macanudas" tabindex="-1" role="dialog"
                                aria-labelledby="scrollmodalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">

                                        <div class="modal-body" style="padding: 0%;">
                                            <br>
                                            <h3 style="text-align: center !important; color: #ffb03b;">¿Por qué
                                                Macanudas?</h3>
                                            <br>
                                            <p style="margin: 0% 15% 1%; text-align: center;">Es un
                                                emprendimiento
                                                matrimonial y, si hay algo que es parte de nuestra tradición y
                                                nuestra
                                                historia familiar, es el gusto por las historietas del dibujante
                                                argentino, Liniers.</p>
                                            <p style="margin: 0% 15% 1%; text-align: center;">Siendo novios, y
                                                después
                                                de casados también, coleccionamos sus libros y todo lo que
                                                encontrábamos
                                                con sus personajes. Parte de nuestro cotillón fueron los
                                                personajes
                                                principales de sus historias.</p>
                                            <p style="margin: 0% 15% 1%; text-align: center;">Cuando surgió este
                                                emprendimiento nos pareció un adjetivo que cerraba por todos
                                                lados:
                                                Habla de nosotros, de nuestros gustos, pero sobre todo de lo que
                                                queremos ser: ¡gente macanuda que hace picadas macanudas!</p>

                                            <br>
                                            <img style="max-width: -webkit-fill-available;"
                                                src="assets/img/casorio_macanudo2.png" alt="">
                                            <br>
                                            <br>
                                            <hr style="width:70%;">
                                            <p style="margin: 0% 15% 1%; text-align: center;" class="font-italic">
                                                <strong> Macanudo: </strong> es la buena persona, alguien
                                                amable,
                                                compañero y que siempre está dispuesto a darte una mano. Es esa
                                                persona
                                                que a donde llega alegra el ambiente. Lo que caracteriza al buen
                                                argentino
                                            </p>
                                            <br>

                                            <div class="text-center">
                                                <button type="button" class="btn btn-outline-secondary rounded-circle"
                                                    data-dismiss="modal">X</button>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h3><strong>Nuestro Objetivo</strong></h3>

                            <p>
                                Nuestro objetivo es destacarnos por la <strong>calidad de los
                                    productos</strong>, y es
                                por ello que para asegurarlo hacemos una selección personal contactándonos de
                                forma
                                <strong>directa con los productores</strong>.
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- End About Section -->

        <!-- ======= Whu Us Section ======= -->
        <section id="why-us" class="why-us">
            <div class="container">
                <div class="section-title">
                    <h2>Nuestros<span> Productos</span></h2>
                    <p>Las mejores combinaciones para tus picadas. Acá te describimos cada una de ellas.</p>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="box" data-toggle="modal" data-target="#fellini">
                            <h1>Picada Fellini</a></h1>
                            <p> <strong>Clásica</strong></p>
                            <br>
                            <img src="assets/img/picadas/clasica.jpg" style="max-width: -webkit-fill-available;">
                        </div>
                    </div>

                    <div class="col-lg-4 mt-4 mt-lg-0">
                        <div class="box" data-toggle="modal" data-target="#picasso">
                            <h1>Picada Picasso</h1>
                            <p><strong> Premium</strong></p>
                            <br>
                            <img src="assets/img/picadas/premium.jpg" style="max-width: -webkit-fill-available;">
                            <br>
                        </div>
                    </div>

                    <div class="col-lg-4 mt-4 mt-lg-0">
                        <div class="box" data-toggle="modal" data-target="#oliverio">
                            <h1>Picada Oliverio</h1>
                            <p><strong>Vegetariana</strong></p>
                            <br>
                            <img src="assets/img/picadas/vegetariana.jpg" style="max-width: -webkit-fill-available;">
                            <br>

                        </div>
                    </div>

                </div>

            </div>
        </section><!-- End Whu Us Section -->

        <div class="modal fade" id="fellini" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">

                    <div class="modal-body">
                        <br>
                        <h3 style="text-align: center !important; color: #ffb03b; margin-bottom: 0%;">Picada
                            Fellini
                        </h3>
                        <p style="text-align: center;">Clásica</p>
                        <br>
                        <div class="card" style="text-align: center; margin: 0% 30% 3%; border: none;">
                            <h4>Quesos:</h4>
                            Sardo | Pategras |Azul |Saborizado Pimienta |Saborizado Oregano |Gouda |Saborizado
                            Aji
                            molido
                            <br>
                            <br>
                            <h4>Fiambres:</h4>
                            Salame Picado grueso | Salame Picado fino | Mortadela | Leberwurst | Jamón Crudo |
                            Bondiola
                            | Lomo ahumado
                            <br>
                            <br>
                            <h4>Además:</h4>
                            Aceitunas | Tomates Cherrys | 2 dip (según disponibilidad de stock)
                            <br>
                            <br>
                            <h4>Pan Casero:</h4>
                        </div>
                        <img style="max-width: -webkit-fill-available; margin: 0% 6% 2%;"
                            src="assets/img/picadas/clasica.jpg" alt="">
                        <br>
                        <br>
                        <div class="text-center">
                            <button type="button" class="btn btn-outline-secondary rounded-circle"
                                data-dismiss="modal">X</button>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="picasso" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">

                    <div class="modal-body">
                        <br>
                        <h3 style="text-align: center !important; color: #ffb03b; margin-bottom: 0%;">Picada
                            Picasso
                        </h3>
                        <p style="text-align: center;">Premium</p>
                        <br>
                        <div class="card" style="text-align: center; margin: 0% 30% 3%; border: none;">
                            <h4>Quesos:</h4>
                            Gruyere | Ahumado | Saborizado Orégano | Canestrato | Semiduro Cabra | Brie | Azul
                            <br>
                            <br>
                            <h4>Fiambres:</h4>
                            Salame Picado grueso | Jamón Crudo | Bondiola | Lomo ahumado | Chorizo seco |
                            Sobrasada |
                            Panceta ahumada
                            <br>
                            <br>
                            <h4>Además:</h4>
                            Aceitunas | Tomates Cherrys | 2 dip (según disponibilidad de stock)
                            <br>
                            <br>
                            <h4>Pan Casero:</h4>
                        </div>
                        <img style="max-width: -webkit-fill-available; margin: 0% 6% 2%;"
                            src="assets/img/picadas/premium.jpg" alt="">
                        <br>
                        <br>
                        <div class="text-center">
                            <button type="button" class="btn btn-outline-secondary rounded-circle"
                                data-dismiss="modal">X</button>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="oliverio" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">

                    <div class="modal-body">
                        <br>
                        <h3 style="text-align: center !important; color: #ffb03b; margin-bottom: 0%;">Picada
                            Oliverio
                        </h3>
                        <p style="text-align: center;">Vegetariana</p>
                        <br>
                        <div class="card" style="text-align: center; margin: 0% 30% 3%; border: none;">
                            <h4>Quesos:</h4>
                            Sardo | Pategrás | Saborizado ají molido | Saborizado Orégano | Canestrato | Azul |
                            Brie |
                            Ahumado | Semiduro Cabra
                            <br>
                            <br>
                            <h4>Además:</h4>
                            Aceitunas | Tomates Cherrys | 2 dip (según disponibilidad de stock)
                            <br>
                            <br>
                            <h4>Pan Casero:</h4>
                        </div>
                        <img style="max-width: -webkit-fill-available; margin: 0% 6% 2%;"
                            src="assets/img/picadas/vegetariana.jpg" alt="">
                        <br>
                        <br>
                        <div class="text-center">
                            <button type="button" class="btn btn-outline-secondary rounded-circle"
                                data-dismiss="modal">X</button>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <!-- ======= Menu Section ======= -->
        <section id="menu" class="menu">
            <div class="container">
                <div class="section-title">
                    <h2>Nuestras <span>Picadas Macanudas</span></h2>
                    <p>Para picar se calcula <strong> 150gr</strong> por persona, para comer <strong>
                            300gr</strong>.
                    </p>
                    <p style="margin-top:0;">Duelas miden Aprox 95cm.</p>
                </div>
                <div id="gallery" class="gallery">
                    <div class="row no-gutters">
                        <div class="col-lg-4 col-md-4">
                            <div class="gallery-item">
                                <p style="text-align: center;">MDF</p>

                                <a href="assets/img/tipo/mdf.jpg" class="venobox" data-gall="gallery-item">
                                    <img src="assets/img/tipo/mdf.jpg" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="gallery-item">
                                <p style="text-align: center;">Duela</p>

                                <a href="assets/img/tipo/duela.jpg" class="venobox" data-gall="gallery-item">
                                    <img src="assets/img/tipo/duela.jpg" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="gallery-item">
                                <p style="text-align: center;">Duela Rectangular</p>

                                <a href="assets/img/tipo/duela unida.jpg" class="venobox" data-gall="gallery-item">
                                    <img src="assets/img/tipo/duela unida.jpg" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <hr>
                <?php 
              $conn = mysqli_connect(
                '31.170.161.22',//31.170.161.22
                'u101685278_pachimanok',// u101685278_pachimanok
                'Pachiman9102',//Pachiman9102
                'u101685278_macanudas' //u101685278_macanudas
              ) or die(mysqli_erro($mysqli));
              ?>
                <div class="row">
                    <div class="col-lg-12 d-flex justify-content-center">
                        <ul id="menu-flters">
                            <li class="filter-active" data-filter="*">Todas</li>
                            <li data-filter=".filter-mdf">MDF</li>
                            <li data-filter=".filter-duelas">Duelas</li>
                            <li data-filter=".filter-duelasUnidas">Duelas Rectangular</li>
                        </ul>
                    </div>
                </div>
                <div class="row menu-container">
                    <!--div class="col-lg-12 menu-item filter-mdf">
            <img src="assets/img/picadas/duela.png" alt=""-->
                    <?php
              
              $query = "SELECT * FROM `picadas` WHERE `type` = 'MDF' and `pagina` = 'si' ORDER BY `picadas`.`description` ASC";
              $result = mysqli_query($conn, $query);  

              while($row = mysqli_fetch_assoc($result)) { 
                
                $title = $row['title'];
                $description = $row['description'];
                $in_ars = $row['in_ars'];
              ?>
                    <div class="col-lg-6 menu-item filter-mdf">
                        <div class="menu-content">
                            <a href="#"><?php echo $title;?></a><span><?php echo '$'. $in_ars;?></span>
                        </div>
                        <div class="menu-ingredients">
                            <?php echo $description;?>
                        </div>
                    </div>
                    <?php } ?>
                    <!--/div-->

                    <?php
            $query = "SELECT * FROM `picadas` WHERE `type` = 'Duela' and `pagina` = 'si' ORDER BY `picadas`.`description` ASC";
            $result = mysqli_query($conn, $query);  
            ?>
                    <!--div class="col-lg-12 menu-item filter-duelas">
            <img src="assets/img/picadas/duela.png" alt=""-->
                    <?php 
              while($row = mysqli_fetch_assoc($result)) { 
            
                $title = $row['title'];
                $description = $row['description'];
                $in_ars = $row['in_ars']; ?>
                    <div class="col-lg-6 menu-item filter-duelas">
                        <div class="menu-content">
                            <a href="#"><?php echo $title;?></a><span><?php echo '$'. $in_ars;?></span>
                        </div>
                        <div class="menu-ingredients">
                            <?php echo $description;?>
                        </div>
                    </div>
                    <?php } ?>
                    <!--/div-->
                    <!--div class="col-lg-12 menu-item filter-duelasUnidas">
            <img src="assets/img/picadas/duela.png" alt=""-->
                    <?php
            $query = "SELECT * FROM `picadas` WHERE `type` = 'Duela Rectangular' and `pagina` = 'si' ORDER BY `picadas`.`description` ASC" ;
            $result = mysqli_query($conn, $query);  
    
            while($row = mysqli_fetch_assoc($result)) { 
            
            $title = $row['title'];
            $description = $row['description'];
            $in_ars = $row['in_ars'];
          
            ?>
                    <div class="col-lg-6 menu-item filter-duelasUnidas">
                        <div class="menu-content">
                            <a href="#"><?php echo $title;?></a><span><?php echo '$'. $in_ars;?></span>
                        </div>
                        <div class="menu-ingredients">
                            <?php echo $description;?>
                        </div>
                    </div>
                    <?php } ?>
                    <!--/div-->
                </div>
            </div>
        </section><!-- End Menu Section -->
        <!-- ======= Events Section ======= -->
        <section id="events" class="events">
            <div class="container">

                <div class="section-title">
                    <h2>Acompañá bien nuestras <span>picadas</span></h2>
                </div>
                <div class="owl-carousel events-carousel">
                    <?php
            $query_add2 = "SELECT * FROM `add2`";
            $result_add2 = mysqli_query($conn, $query_add2);  
    
            while($row = mysqli_fetch_assoc($result_add2)) { 
            
              $img = $row['img'];
              $title= $row['title'];
              $proveedor = $row['proveedor'];
              $description = $row['description'];
              $in_ars = $row['in_ars'];

          ?>
                    <div class="row event-item">
                        <div class="col-lg-6">
                            <img src="<?php echo $img;?>" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-6 pt-4 pt-lg-0 content">
                            <h2><?php echo $title;?></h2>

                            <p class="font-italic">by: <span><?php echo $proveedor;?></span></p>
                            <p>
                                <br>
                                <?php echo $description;?>
                            </p>
                            <h3>$ <strong><?php echo $in_ars;?></strong></h3>
                        </div>
                    </div>

                    <!--div class="row event-item">
            <div class="col-lg-6">
              <img src="assets/img/slide/Macanudas-112.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-lg-6 pt-4 pt-lg-0 content">
              <h3>Porter</h3>
              <p>Cerveza artesanal de Black Snake</p>         

              <p class="font-italic">
                 Cerveza irlandeza, suave, cremosa y seca.  
              </p>
              <ul>
                <li> <strong> Por qué nos acompañan:</strong></li>
                <li><i class="icofont-check-circled"></i>Porque les gusta la cerveza y tiene una gran canitdad de variades ideales para maridar con nuestras picadas.</li>
                <li><i class="icofont-check-circled"></i> Cream Ale - Kölsch - Scotish Ale</li>
                
              </ul>
             
            </div>
          </div>
          <div class="row event-item">
            <div class="col-lg-6">
              <img src="assets/img/slide/PM-8.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-lg-6 pt-4 pt-lg-0 content">
              <h3>Chipá Congelado </h3>
              <p>by: Cheese-pá</p>
              <p>
                Te ofrecemos 250 gr del mejor chipá de Mendoza de nuestros amigos de Cheese-pá. Te lo mandamos con las indicaciones para hornearlos y comerlos bien calentitos
              </p>
              <div class="row">
                <div class="col-5 mx-auto">
              <img class="img-fluid" src="assets/img/chesse-pa.png" alt="">
              </div>
              
              </div>
              ul>
                <li> <strong> Por qué nos acompañan:</strong></li>
                <li><i class="icofont-check-circled"></i>Vino de Familia Mendocina hecho con los standaders de calidad de exportación pero de fomrma arrezanal.</li>
                <li><i class="icofont-check-circled"></i> Cream Ale - Kölsch - Scotish Ale</li>
              </ul>
            </div>
          </div-->
                    <?php
          }
          ?>
                </div>
        </section>
        <section id="specials" class="specials">
            <div class="container">
                <div class="section-title pb-0">
                    <h2>La opinion de nuestros <span>Clientes Felices</span></h2>
                    <!--p>Algunos comentarios de nuestros Clientes:</p-->
                    <hr>
                </div>
                <div class="owl-carousel events-carousel">
                    <?php
                        $query_satisfaction = "SELECT satisfaction_servicio, product, cnee, comentario_pagina  FROM `general` WHERE pagina = 'si' ORDER BY created_at DESC LIMIT 5";
                        $result_satifaction = mysqli_query($conn, $query_satisfaction);  
                
                        while($rows = mysqli_fetch_assoc($result_satifaction)) { 
                        
                        $servicio = $rows['satisfaction_servicio'];
                        $profuct= $rows['product'];
                        $cnee = $rows['cnee'];
                        $comentario = $rows['comentario_pagina'];

                            if($servicio == 1){
                                $servicio = '<i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-secondary p-1" ></i><i class="fa fa-star text-secondary p-1" ></i><i class="fa fa-star text-secondary p-1" ></i><i class="fa fa-star text-secondary " ></i>';
                            }elseif($servicio == 2){
                                $servicio = '<i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-secondary p-1" ></i><i class="fa fa-star text-secondary p-1" ></i><i class="fa fa-star text-secondary " ></i>';

                            }elseif($servicio == 3){
                                $servicio = '<i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-secondary p-1" ></i><i class="fa fa-star text-secondary " ></i>';

                            }elseif($servicio == 4){
                                $servicio = '<i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-secondary " ></i>';

                            }else{
                                $servicio = '<i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning p-1" ></i></i><i class="fa fa-star text-warning p-1" ></i><i class="fa fa-star text-warning " ></i>';

                            }
                    ?>
                    <div class="row event-item">
                        <div class="col-lg-8 mx-auto">
                            <div class="row">
                                <div class="col-sm-3 mx-auto" style="text-align: center;">
                                    <?php echo $servicio ;?>
                                </div>
                            </div>
                            <br>
                            <p style="text-align: center;"><?php echo $comentario; ?></p>
                            <p class="font-italic mb-0" style="text-align: center;">
                                <strong><?php echo $cnee; ?></strong></span>
                            </p>
                            <p class="font-italic" style="text-align: center;"><?php echo $profuct; ?></span></p>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <hr>
            </div>

        </section><!-- End Events Section -->
        <!-- ======= Chefs Section ======= -->
        <!-- <section id="chefs" class="chefs">
            <div class="container">
                <div class="section-title">
                    <h2>Streaming con <span>Delivery</span></h2>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="member">
                            <div class="pic">
                                <img src="assets/img/slide/casciari2.png" class="img-fluid" alt="">
                            </div>
                            <div class="member-info">
                                <h4>Hernán Casciari</h4>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <p style="font-weight:600;"><span>Todos los Sábados de Cuarentena</span> 22hs.</p>
                        <p style="margin: 0;"><span>Cualquiera de nuestras opciones + </span> $400 </p>
                        <p>te llevamos tu picada y disfrutás del streaming de Hernán</p>
                        <br>
                        <h3 style="color: #ffb03b;"><strong>Hernán Casciari</strong></h3>
                        <br>
                        <p>Nació en Mercedes, Buenos Aires, en 1972. Fundó la Editorial <strong>Orsai</strong> y
                            dirige
                            la revista Orsai(crónica periodística) y la revista <strong>Bonsai</strong>
                            (infantil).</p>
                        <p>Publicó, entre otras novelas, <strong>El pibe que arruinaba las fotos</strong> y
                            <strong>Más
                                respeto que soy tu madre</strong>; y libros de cuentos como <strong>España decí
                                alpiste</strong> y <strong>Messi es un perro</strong>.
                        </p>
                        <p>En 2012 empezó a leer cuentos en radios - <strong>Vorterix</strong>("Tenemos malas
                            noticias")
                            y <strong>Metro</strong> ("Perros de la calle")-; el éxito de esas lecturas hizo que
                            comenzara a leer sus cuentos en televisión <strong>-Telefé-</strong> ("Staff a la
                            medianoche") y también hacer lectura de cuentos de teatros.</p>
                        <div class="text-center">
                            <button type="submit" class="btn btn-warning" data-toggle="modal" data-target="#cuentos">uno
                                de sus cuentos</button>
                        </div>
                    </div>
                </div>
            </div>
        </section> --><!-- End About Section -->
        <div class="modal fade" id="cuentos" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <iframe width="760" height="665" src="https://www.youtube.com/embed/sVuMmcFu9uI" frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                        <div class="text-center">
                            <button type="button" class="btn btn-warning" data-dismiss="modal">cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- ======= Specials Section ======= -->
        <section id="specials" class="specials">
            <div class="container">
                <div class="section-title">
                    <h2>Picadas Macanudas en <span>los medios</span></h2>
                    <p>Estamos en los medios y participamos de diferentes eventos.</p>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <ul class="nav nav-tabs flex-column">
                            <li class="nav-item">
                                <a class="nav-link active show" data-toggle="tab" href="#tab-1">Matices del
                                    Vino</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab-2">La Peña del tano</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab-3">La enoteca</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab-4">Metro Mendoza</a>
                            </li>
                            <!--li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tab-4">Nostrum qui quasi</a>
              </li-->
                            <!--li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tab-5">Iusto ut expedita aut</a>
              </li-->
                        </ul>
                    </div>
                    <div class="col-lg-9 mt-4 mt-lg-0">
                        <div class="tab-content">
                            <div class="tab-pane active show" id="tab-1">
                                <div class="row">
                                    <div class="col-lg-8 details order-2 order-lg-1">
                                        <h3>Matices del Vino</h3>
                                        <p class="font-italic">Radio La Red 94.1 Sábados de 12 a 14hs.</p>
                                        <p>Programa de Radio relacionado al mundo del vino, los sábados de 12 a
                                            14hs por
                                            La Red Mendoza 94.1.</p>
                                    </div>
                                    <div class="col-lg-4 text-center order-1 order-lg-2">
                                        <img src="assets/img/specials-1.jpg" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-4">
                                <div class="row">
                                    <div class="col-lg-8 details order-2 order-lg-1">
                                        <h3>Metro Mendoza 95.5</h3>
                                        <p class="font-italic">Nos encontrarás durante toda la programación.</p>
                                    </div>
                                    <div class="col-lg-4 text-center order-1 order-lg-2">
                                        <a href="https://metromendoza.com.ar" target="_blank">
                                            <img src="assets/img/lametro.png" alt="" class="img-fluid">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-2">
                                <div class="row">
                                    <div class="col-lg-8 details order-2 order-lg-1">
                                        <h3>La Peña del tano</h3>
                                        <p>Desde Floyd tv online, con la conducción de Tano Robles y Lisandro
                                            Bertín nos
                                            acompañan un sábado al mes con Folklore, entrevistas y humor para
                                            disfrutar
                                            en Familia en esta cuarentena. Y nosotros los acompañamos a ellos
                                            para que
                                            puedan degustar de nuestras tablas.</p>
                                    </div>
                                    <div class="col-lg-4 text-center order-1 order-lg-2">
                                        <img src="assets/img/penadeltona.png" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-3">
                                <div class="row">
                                    <div class="col-lg-8 details order-2 order-lg-1">
                                        <h3>La enoteca</h3>
                                        <p>La Enoteca, emplazada en el Centro Cívico de Mendoza es el único
                                            testimonio
                                            arquitectónico existente de la Escuela Nacional de Vitivinicultura.
                                            Aquí se
                                            formaron los primeros profesionales de la vitivinicultura de
                                            Argentina y
                                            Latinoamérica. En la actualidad, esta antigua bodega ha sido
                                            refuncionalizada y es el Centro Temático del Vino donde se
                                            desarrollan
                                            actividades de promoción de la actividad vitivinícola durante todo
                                            el año.
                                        </p>
                                        <p>Junto a ellos participamos de encuentros como: </p>
                                        <p style="margin-left:3%;"> - Vino & Quesos, donde se realizó un
                                            recorrido de
                                            puro placer maridando 6 de nuestras variedades de quesos con 3
                                            vinos.</p>
                                        <p style="margin-left:3%;"> - Vino & Picada, allí se aprendieron
                                            conceptos
                                            básicos de maridaje y cómo elegir el mejor vino para cada picada.
                                        </p>
                                    </div>
                                    <div class="col-lg-4 text-center order-1 order-lg-2">
                                        <img src="assets/img/specials-3.jpg" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-4">
                                <div class="row">
                                    <div class="col-lg-8 details order-2 order-lg-1">
                                        <h3>Fuga dolores inventore laboriosam ut est accusamus laboriosam dolore
                                        </h3>
                                        <p class="font-italic">Totam aperiam accusamus. Repellat consequuntur
                                            iure
                                            voluptas iure porro quis delectus</p>
                                        <p>Eaque consequuntur consequuntur libero expedita in voluptas. Nostrum
                                            ipsam
                                            necessitatibus aliquam fugiat debitis quis velit. Eum ex maxime
                                            error in
                                            consequatur corporis atque. Eligendi asperiores sed qui veritatis
                                            aperiam
                                            quia a laborum inventore</p>
                                    </div>
                                    <div class="col-lg-4 text-center order-1 order-lg-2">
                                        <img src="assets/img/specials-4.jpg" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-5">
                                <div class="row">
                                    <div class="col-lg-8 details order-2 order-lg-1">
                                        <h3>Est eveniet ipsam sindera pad rone matrelat sando reda</h3>
                                        <p class="font-italic">Omnis blanditiis saepe eos autem qui sunt debitis
                                            porro
                                            quia.</p>
                                        <p>Exercitationem nostrum omnis. Ut reiciendis repudiandae minus. Omnis
                                            recusandae ut non quam ut quod eius qui. Ipsum quia odit vero atque
                                            qui
                                            quibusdam amet. Occaecati sed est sint aut vitae molestiae voluptate
                                            vel</p>
                                    </div>
                                    <div class="col-lg-4 text-center order-1 order-lg-2">
                                        <img src="assets/img/specials-5.jpg" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- End Specials Section -->



        <!-- ======= Book A Table Section ======= -->
        <!--section id="book-a-table" class="book-a-table">
      <div class="container">
        <div class="section-title">
          <h2>Book a <span>Table</span></h2>
          <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p>
        </div>

        <form action="forms/book-a-table.php" method="post" role="form" class="php-email-form">
          <div class="form-row">
            <div class="col-lg-4 col-md-6 form-group">
              <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
              <div class="validate"></div>
            </div>
            <div class="col-lg-4 col-md-6 form-group">
              <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email">
              <div class="validate"></div>
            </div>
            <div class="col-lg-4 col-md-6 form-group">
              <input type="text" class="form-control" name="phone" id="phone" placeholder="Your Phone" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
              <div class="validate"></div>
            </div>
            <div class="col-lg-4 col-md-6 form-group">
              <input type="text" name="date" class="form-control" id="date" placeholder="Date" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
              <div class="validate"></div>
            </div>
            <div class="col-lg-4 col-md-6 form-group">
              <input type="text" class="form-control" name="time" id="time" placeholder="Time" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
              <div class="validate"></div>
            </div>
            <div class="col-lg-4 col-md-6 form-group">
              <input type="number" class="form-control" name="people" id="people" placeholder="# of people" data-rule="minlen:1" data-msg="Please enter at least 1 chars">
              <div class="validate"></div>
            </div>
          </div>
          <div class="form-group">
            <textarea class="form-control" name="message" rows="5" placeholder="Message"></textarea>
            <div class="validate"></div>
          </div>
          <div class="mb-3">
            <div class="loading">Loading</div>
            <div class="error-message"></div>
            <div class="sent-message">Your booking request was sent. We will call back or send an Email to confirm your reservation. Thank you!</div>
          </div>
          <div class="text-center"><button type="submit">Send Message</button></div>
        </form>

      </div>
    </section-->
        <!-- End Book A Table Section -->

        <!-- ======= Gallery Section ======= -->
        <section id="gallery" class="gallery">
            <div class="container-fluid">

                <div class="section-title">
                    <h2>Algunas imágenes de nuestros <span>Productos</span></h2>
                    <p>Sabores que compartimos</p>
                </div>

                <div class="row no-gutters">



                    <div class="col-lg-3 col-md-4">
                        <div class="gallery-item">
                            <a href="assets/img/gallery/gallery-2.jpg" class="venobox" data-gall="gallery-item">
                                <img src="assets/img/slide/Galeria/Macanudas-20.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="gallery-item">
                            <a href="assets/img/gallery/gallery-3.jpg" class="venobox" data-gall="gallery-item">
                                <img src="assets/img/slide/Galeria/Macanudas-44.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="gallery-item">
                            <a href="assets/img/gallery/gallery-1.jpg" class="venobox" data-gall="gallery-item">
                                <img src="assets/img/slide/Galeria/Macanudas-106.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="gallery-item">
                            <a href="assets/img/gallery/gallery-4.jpg" class="venobox" data-gall="gallery-item">
                                <img src="assets/img/slide/Galeria/Macanudas-15.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>


                    <div class="col-lg-3 col-md-4">
                        <div class="gallery-item">
                            <a href="assets/img/gallery/gallery-5.jpg" class="venobox" data-gall="gallery-item">
                                <img src="assets/img/slide/Galeria/Macanudas-64.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="gallery-item">
                            <a href="assets/img/gallery/gallery-6.jpg" class="venobox" data-gall="gallery-item">
                                <img src="assets/img/slide/Galeria/Macanudas-67.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="gallery-item">
                            <a href="assets/img/gallery/gallery-7.jpg" class="venobox" data-gall="gallery-item">
                                <img src="assets/img/slide/Galeria/Macanudas-74.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-4">
                        <div class="gallery-item">
                            <a href="assets/img/gallery/gallery-8.jpg" class="venobox" data-gall="gallery-item">
                                <img src="assets/img/slide/Galeria/Macanudas-92.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                    </div>

                </div>

            </div>
        </section><!-- End Gallery Section -->



        <!-- ======= Testimonials Section ======= -->
        <!--section id="testimonials" class="testimonials">
      <div class="container">

        <div class="owl-carousel testimonials-carousel">

          <div class="testimonial-item">
            <img src="assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="">
            <h3>Saul Goodman</h3>
            <h4>Ceo &amp; Founder</h4>
            <div class="stars">
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
            </div>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt="">
            <h3>Sara Wilsson</h3>
            <h4>Designer</h4>
            <div class="stars">
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
            </div>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="assets/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt="">
            <h3>Jena Karlis</h3>
            <h4>Store Owner</h4>
            <div class="stars">
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
            </div>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="assets/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt="">
            <h3>Matt Brandon</h3>
            <h4>Freelancer</h4>
            <div class="stars">
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
            </div>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

          <div class="testimonial-item">
            <img src="assets/img/testimonials/testimonials-5.jpg" class="testimonial-img" alt="">
            <h3>John Larson</h3>
            <h4>Entrepreneur</h4>
            <div class="stars">
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
              <i class="icofont-star"></i>
            </div>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              Quis quorum aliqua sint quem legam fore sunt eram irure aliqua veniam tempor noster veniam enim culpa labore duis sunt culpa nulla illum cillum fugiat legam esse veniam culpa fore nisi cillum quid.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>

        </div>

      </div>
    </section-->
        <!-- End Testimonials Section -->

        <!-- ======= Contact Section ======= -->
        <section id="contact" class="contact">
            <div class="container">

                <div class="section-title">
                    <h2><span>Contactanos</span></h2>
                    <p>Por cualquiera de estos medios o dejanos un mensaje.</p>
                </div>
            </div>

            <div class="map">

                <iframe style="border:0; width: 100%; height: 350px;"
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13398.88694727627!2d-68.81595921488713!3d-32.905524643435726!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x967e095d1db5e213%3A0xbc752ee160420b8!2sAmado%20Nervo%2C%20Mendoza!5e0!3m2!1ses!2sar!4v1598278104723!5m2!1ses!2sar"
                    frameborder="0" allowfullscreen></iframe>
            </div>

            <div class="container mt-5">

                <div class="info-wrap">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 info">
                            <i class="icofont-google-map"></i>
                            <h4>Localización:</h4>
                            <p>Amado Nervo y Dorrego <br>Mendoza, Argentina</p>
                        </div>

                        <div class="col-lg-3 col-md-6 info mt-4 mt-lg-0">
                            <i class="icofont-clock-time icofont-rotate-90"></i>
                            <h4>Horarios:</h4>
                            <p>Lunes-Sabado:<br>10:00 AM - 20:00 PM</p>
                        </div>


                        <div class="col-lg-3 col-md-6 info mt-4 mt-lg-0">
                            <i class="icofont-phone"></i>
                            <h4>Whats App:</h4>
                            <p>+54 9 2613 40-5502<br></p>
                        </div>
                    </div>
                </div>

                <!--form action="forms/contact.php" method="post" role="form" class="php-email-form">
          <div class="form-row">
            <div class="col-md-6 form-group">
              <input type="text" name="name" class="form-control" id="name" placeholder="Tu nombre" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
              <div class="validate"></div>
            </div>
            <div class="col-md-6 form-group">
              <input type="email" class="form-control" name="email" id="email" placeholder="tu@correo" data-rule="email" data-msg="Please enter a valid email" />
              <div class="validate"></div>
            </div>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="subject" id="subject" placeholder="Asunto" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
            <div class="validate"></div>
          </div>
          <div class="form-group">
            <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Tus comentarios"></textarea>
            <div class="validate"></div>
          </div>
          <div class="mb-3">
            <div class="loading">Loading</div>
            <div class="error-message"></div>
            <div class="sent-message">Your message has been sent. Thank you!</div>
          </div>
          <div class="text-center"><button type="submit">Send Message</button></div>
        </form-->

            </div>
        </section><!-- End Contact Section -->

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <footer id="footer">
        <div class="container">
            <a href="index.php"><img style="width: 25%;" src="assets/img/MCND_Logo_1080_WHITE.png" alt=""></a>
            <div class="social-links">
                <a href="https://api.whatsapp.com/send?phone=542613405502&text=Hola,%20te%20contacto%20desde%20la%20Página"
                    class="twitter"><i class="bx bxl-whatsapp"></i></a>
                <a href="https://www.facebook.com/picadasmacanudas" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="https://www.instagram.com/picadasmacanudas" class="instagram"><i
                        class="bx bxl-instagram"></i></a>

            </div>
            <div class="copyright">
                &copy; Copyright <strong><span>Picadas Macanudas</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/delicious-free-restaurant-bootstrap-theme/ -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> & <a href="">BUILDIT</a>
            </div>
        </div>
    </footer><!-- End Footer -->
    <a href="https://api.whatsapp.com/send?phone=542613405502" target="_blank" style="color:#433f39; text-align:center;"
        class="whatsapp"><i class="bx bxl-whatsapp"></i>Contactanos!</a>
    <!--a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a-->

    <!-- Vendor JS Files -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>
    <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/venobox/venobox.min.js"></script>
    <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
    <script>
    $(function() {
        $('[data-toggle="popover"]').popover()
    })
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
    </script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>

</html>