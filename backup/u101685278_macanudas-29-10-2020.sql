-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-10-2020 a las 15:05:53
-- Versión del servidor: 10.4.14-MariaDB-cll-lve
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `u101685278_macanudas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `add2`
--

CREATE TABLE `add2` (
  `id` int(11) NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `q` int(11) NOT NULL,
  `proveedor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_ars` decimal(11,2) NOT NULL,
  `out_ars` decimal(11,2) NOT NULL,
  `createrd_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `add2`
--

INSERT INTO `add2` (`id`, `img`, `title`, `description`, `q`, `proveedor`, `in_ars`, `out_ars`, `createrd_at`, `user`) VALUES
(4, 'assets/img/add2/chipa.jpg', 'Chipa Congelado (250 gr)', 'Te ofrecemos 250 gr del mejor chipá de Mendoza de nuestros amigos de Cheese-pá. Te lo mandamos con las indicaciones para hornearlos y comerlos bien calentitos.', 3, 'Chesse-pa', '250.00', '0.00', '2020-10-29 15:04:19', ''),
(5, 'assets/img/add2/cream-Ale.jpg', 'Cerveza Cream', 'Es una cerveza delicada, suave y cremosa. Llena la boca con un leve dulce al final', 14, 'Black Snake', '200.00', '0.00', '2020-10-29 13:26:14', ''),
(6, 'assets/img/add2/cream-Ale.jpg', 'Cerveza Golden', 'Clásica Ale dorada, robusta ,balanceada, con personalidad.', 6, 'Black Snake', '200.00', '0.00', '2020-10-08 22:14:10', ''),
(7, 'assets/img/add2/kolsch.jpg', 'Cerveza Kolsh', 'Cerveza de colonia (Alemania). Son menos agresivamente amargas y un dejo frutado similar a las Ale es una cerveza delicada y suave.', 4, 'Black Snake', '200.00', '0.00', '2020-10-08 22:14:20', ''),
(8, 'assets/img/add2/porter.jpg', 'Cerveza Porter', 'Cerveza Irlandesa, suave, cremosa y seca.', 11, 'Black Snake', '200.00', '0.00', '2020-10-08 22:14:31', ''),
(9, 'assets/img/add2/indina-Pale-Ale.jpg', 'Cerveza IPA', 'De color ámbar profundo/cobrizo ,predominan el sabor de lúpulo y malta. Nació cuando el imperio ingles se encontraba en India. Como medio de conservación agregaban grandes cantidades de lúpulo ,haciéndola un clásico Británico, Casi intolerable para los tomadores principiantes de este estilo y adictiva para los veteranos .Genera amores y odio pero nunca indiferencia.', 0, 'Black Snake', '230.00', '0.00', '2020-10-08 22:14:42', ''),
(10, 'assets/img/add2/scotish-Ale.jpg', 'Cerveza Scottish Ale', 'Enfatiza la malta dulce y redondeando.de color oscuro rojizo con leves notas de malta tostada, es una Ale fuerte, con carácter de malta, típico de las Scottish Ales con Alta complejidad de sabores (flavors).', 10, 'Black Snake', '200.00', '0.00', '2020-10-08 22:14:55', ''),
(11, 'assets/img/add2/pacto-malbec.jpg', 'Vino Pacto Malbec', 'Los pactos deben ser cumplidos y honrados, como las amistades y el trabajo. Vinos de autor. Leo, Fran y Rodo', 12, 'Pacto Wine', '500.00', '0.00', '2020-10-08 22:15:59', ''),
(12, 'assets/img/add2/pacto-1.jpg', 'Vino Pacto Cabernet Franc', 'Los pactos deben ser cumplidos y honrados, como las amistades y el trabajo. Vinos de autor. Leo, Fran y Rodo', 6, 'Pacto Wine', '700.00', '0.00', '2020-10-29 15:04:34', ''),
(13, 'assets/img/add2/chocotorta.jpg', 'Chocotorta (300 gr)', 'El postre que nunca te va a fallar de nuestra amiga de Melmél', 0, 'MelMel', '300.00', '0.00', '2020-10-08 22:15:31', ''),
(14, 'assets/img/add2/dulces.jpg', 'Mix de Chocolates', 'Si la querés hacer completa regalá o regalate un mix de de chocolates que incluye:<br>50 gr de maní bañado en chocolate,<br>50 gr granos de café bañados en chocolate,<br>50 gr naranja bañada en chocolate,<br>50 gr de confites de chocolate,<br>50 gr de almendras bañadas en chocolate blanco', 5, 'Caro Carballo', '500.00', '0.00', '2020-10-08 22:15:48', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `add3`
--

CREATE TABLE `add3` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `q` int(11) NOT NULL,
  `proveedor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_ars` decimal(11,2) NOT NULL,
  `out_ars` decimal(11,2) NOT NULL,
  `createrd_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `add4`
--

CREATE TABLE `add4` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `q` int(11) NOT NULL,
  `proveedor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_ars` decimal(11,2) NOT NULL,
  `out_ars` decimal(11,2) NOT NULL,
  `createrd_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `add5`
--

CREATE TABLE `add5` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `q` int(11) NOT NULL,
  `proveedor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_ars` decimal(11,2) NOT NULL,
  `out_ars` decimal(11,2) NOT NULL,
  `createrd_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `add6`
--

CREATE TABLE `add6` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `q` int(11) NOT NULL,
  `proveedor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_ars` decimal(11,2) NOT NULL,
  `out_ars` decimal(11,2) NOT NULL,
  `createrd_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `delivery`
--

CREATE TABLE `delivery` (
  `id` int(11) NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `km_to_zero` decimal(11,2) NOT NULL,
  `px_km` decimal(11,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `delivery`
--

INSERT INTO `delivery` (`id`, `location`, `km_to_zero`, `px_km`, `created_at`, `user`) VALUES
(1, 'Capital - Ciudad Capital', '5.00', '180.00', '0000-00-00 00:00:00', ''),
(2, 'Godoy Cruz - Ciudad de Godoy Cruz', '5.00', '180.00', '0000-00-00 00:00:00', ''),
(3, 'Guaymallen - Belgrano', '5.00', '180.00', '0000-00-00 00:00:00', ''),
(4, 'Guaymallen - Bermejo', '10.00', '220.00', '0000-00-00 00:00:00', ''),
(5, 'Guaymallen - Buena Nueva', '15.00', '260.00', '0000-00-00 00:00:00', ''),
(6, 'Guaymallen - Capilla del Rosario', '10.00', '220.00', '0000-00-00 00:00:00', ''),
(8, 'Guaymallen - Colonia Segovia', '15.00', '260.00', '0000-00-00 00:00:00', ''),
(9, 'Guaymallen - Dorrego', '1.00', '180.00', '0000-00-00 00:00:00', ''),
(10, 'Guaymallen - El Sauce', '10.00', '220.00', '0000-00-00 00:00:00', ''),
(11, 'Guaymallen - Jesus Nazareno', '5.00', '180.00', '0000-00-00 00:00:00', ''),
(12, 'Guaymallen - kilometro 11', '15.00', '260.00', '0000-00-00 00:00:00', ''),
(13, 'Guaymallen - kilometro 8', '15.00', '260.00', '0000-00-00 00:00:00', ''),
(14, 'Guaymallen - La Primavera (G)', '20.00', '300.00', '0000-00-00 00:00:00', ''),
(15, 'Guaymallen - Las Cañas', '5.00', '180.00', '0000-00-00 00:00:00', ''),
(16, 'Guaymallen - Los Corralitos', '20.00', '300.00', '0000-00-00 00:00:00', ''),
(17, 'Guaymallen - Nueva Ciudad', '5.00', '180.00', '0000-00-00 00:00:00', ''),
(18, 'Guaymallen - Pedro Molina', '5.00', '180.00', '0000-00-00 00:00:00', ''),
(19, 'Guaymallen - Puente de Hierro', '20.00', '300.00', '0000-00-00 00:00:00', ''),
(20, 'Guaymallen - Rodeo de la Cruz', '10.00', '220.00', '0000-00-00 00:00:00', ''),
(21, 'Guaymallen - San Francisco del Monte', '5.00', '180.00', '0000-00-00 00:00:00', ''),
(22, 'Guaymallen - San Jose', '5.00', '180.00', '0000-00-00 00:00:00', ''),
(23, 'Guaymallen - Villa Nueva', '10.00', '220.00', '0000-00-00 00:00:00', ''),
(24, 'Las Heras - Capdeville', '30.00', '340.00', '0000-00-00 00:00:00', ''),
(25, 'Las Heras - Ciudad de Las Heras', '10.00', '220.00', '0000-00-00 00:00:00', ''),
(26, 'Las Heras - El Algarrobal', '20.00', '300.00', '0000-00-00 00:00:00', ''),
(28, 'Las Heras - El Challao', '15.00', '260.00', '0000-00-00 00:00:00', ''),
(30, 'Las Heras - El Pastal', '30.00', '340.00', '0000-00-00 00:00:00', ''),
(31, 'Las Heras - El Plumerillo', '10.00', '220.00', '0000-00-00 00:00:00', ''),
(32, 'Las Heras - El Zapallar', '10.00', '220.00', '0000-00-00 00:00:00', ''),
(33, 'Las Heras - Panqueua', '15.00', '260.00', '0000-00-00 00:00:00', ''),
(34, 'Lujan - Agrelo', '30.00', '340.00', '0000-00-00 00:00:00', ''),
(35, 'Lujan - Carrodilla', '10.00', '220.00', '0000-00-00 00:00:00', ''),
(36, 'Lujan - Chacras de Coria', '10.00', '220.00', '0000-00-00 00:00:00', ''),
(37, 'Lujan - Ciudad de Lujan', '20.00', '300.00', '0000-00-00 00:00:00', ''),
(38, 'Lujan - La Puntilla', '15.00', '260.00', '0000-00-00 00:00:00', ''),
(39, 'Lujan - Las Compuertas', '30.00', '340.00', '0000-00-00 00:00:00', ''),
(40, 'Lujan - Mayor Drummond', '15.00', '260.00', '0000-00-00 00:00:00', ''),
(43, 'Lujan - Perdriel', '30.00', '340.00', '0000-00-00 00:00:00', ''),
(44, 'Lujan - Vistalba', '20.00', '300.00', '0000-00-00 00:00:00', ''),
(45, 'Maipu- Barrancas', '30.00', '340.00', '0000-00-00 00:00:00', ''),
(46, 'Maipu- Ciudad de MaipU', '10.00', '220.00', '0000-00-00 00:00:00', ''),
(47, 'Maipu- Coquimbito', '15.00', '260.00', '0000-00-00 00:00:00', ''),
(48, 'Maipu- Cruz de Piedra', '30.00', '340.00', '0000-00-00 00:00:00', ''),
(50, 'Maipu- Fray Luis Betlran', '30.00', '340.00', '0000-00-00 00:00:00', ''),
(51, 'Maipu- General Gutierrez', '10.00', '220.00', '0000-00-00 00:00:00', ''),
(53, 'Maipu- Lunlunta', '20.00', '300.00', '0000-00-00 00:00:00', ''),
(54, 'Maipu- Luzuriaga', '10.00', '220.00', '0000-00-00 00:00:00', ''),
(55, 'Maipu- Rodeo del Medio', '20.00', '300.00', '0000-00-00 00:00:00', ''),
(57, 'Maipu- Russel', '20.00', '300.00', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `delivery_hour`
--

CREATE TABLE `delivery_hour` (
  `id` int(11) NOT NULL,
  `hour_delivery` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createrd_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `delivery_hour`
--

INSERT INTO `delivery_hour` (`id`, `hour_delivery`, `delivery`, `createrd_at`, `user`) VALUES
(1, 'de 10 a 13', 'con_retiro', '2020-09-15 18:41:22', 0),
(2, 'de 17 a 20', 'con_retiro', '2020-09-15 18:41:26', 0),
(3, '10:30 - 13:30', 'con_envio', '2020-09-15 18:46:34', 0),
(4, '14:00 - 17:00', 'con_envio', '2020-09-15 18:46:34', 0),
(5, '18:00 - 20:00', 'con_envio', '2020-09-15 18:46:34', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `general`
--

CREATE TABLE `general` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cel_phone` bigint(40) NOT NULL,
  `cnee` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cnee_cel_phone` bigint(40) NOT NULL,
  `inscription` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_date` date NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referencia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add4` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add5` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `add6` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule_available` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_mode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_ars` decimal(11,2) NOT NULL,
  `out_ars` decimal(11,2) NOT NULL,
  `satisfaction_product` int(1) NOT NULL,
  `satisfaction_servicio` int(1) NOT NULL,
  `satisfaction_entrega` int(1) NOT NULL,
  `satisfaction_observation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_pago` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `general`
--

INSERT INTO `general` (`id`, `type`, `customer`, `cel_phone`, `cnee`, `cnee_cel_phone`, `inscription`, `delivery_date`, `address`, `location`, `referencia`, `product`, `add1`, `add2`, `add3`, `add4`, `add5`, `add6`, `schedule_available`, `payment_mode`, `in_ars`, `out_ars`, `satisfaction_product`, `satisfaction_servicio`, `satisfaction_entrega`, `satisfaction_observation`, `status`, `status_pago`, `created_at`) VALUES
(7, 'con_envio', 'Martina Matiazzo', 2616222034, 'Camila Roby', 2147483647, 'Feliz cumpleaños amiga!! Disfruta mucho! Te amamos❤️ Marti, Coni y Jus', '2020-10-13', 'Barrio Palmares Open Mall, Calle Napoles, Flia Roby', 'Godoy Cruz - Ciudad de Godoy Cruz', 'Los de la guardia te indican como llegar al domicilio! ', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '10:30 - 13:30', 'Efectivo', '1100.00', '916.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-08 18:42:06'),
(10, 'con_envio', 'Federico Pippi', 2615700861, 'Santiago Saieg', 2147483647, 'Muy feliz cumple Santi, de Lillian y Fede', '2020-10-10', 'Belgrano 638 dpto 2', 'Godoy Cruz - Ciudad de Godoy Cruz', 'Entre Adolfo Calle y Jean Jaures', 'Picada Fellini (Clásica)', '4', '-Cerveza Cream', 'MDF', '', '', '', '14:00 - 17:00', 'Mercado Pago', '1300.00', '1076.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-08 21:28:29'),
(11, 'con_envio', 'Emiliano Menechelli', 2634537093, 'Lucas Carbo ', 261, '¡Feliz cumple Lu! Cariños de Todos los Menechelli', '2020-10-09', 'Manzana 13 Casa 23 Puerta 5 Barrio: Dalvian ', 'Las Heras - El Challao', 'Barrio Dalvian ', 'Picada Fellini (Clásica)', '5', 'Cerveza Cream', 'MDF', '', '', '', 'de 18:00 - 20:00', 'Mercado Pago', '1580.00', '1240.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-08 23:39:36'),
(13, 'con_envio', 'Marcela Pereyra', 2614724340, 'Melo Orlando ', 2147483647, 'Melo: “Gracias por cuidarnos con tanto cariño !!!❤️❤️“', '2020-10-10', 'Huarpes 96. ', 'Capital - Ciudad Capital', '', 'Picada Picasso (Premium)', '4', '', 'Duela', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1960.00', '1604.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-09 00:04:01'),
(14, 'con_envio', 'Fernanda sandes', 2616114305, 'María Sandes ', 155880832, 'Feliz día ma, te amamos ❤️', '2020-10-18', 'Virgen de las Nieves 3803', 'Guaymallen - Pedro Molina', 'Esquina roja diagonal a la escuela ', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '10:30 - 13:30', 'Efectivo', '1100.00', '916.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-09 12:58:28'),
(16, 'con_envio', 'Claudia Stabio', 2615082117, 'Mauricio Zinna', 54, 'Muy feliz cumple Mauri. Te queremos. Familias Ruiz, Rofrano, Juli y Anita. ', '2020-10-12', 'Vicente López y Planes 1031', 'Lujan - Ciudad de Lujan', 'Entre Richieri y Vergara, al lado de una casa rosa', 'Picada Fellini (Clásica)', '4', '-Cerveza Kolsh', 'Duela', '', '', '', '10:30 - 13:30', '', '1920.00', '1436.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-09 15:18:44'),
(19, 'con_envio', 'Cecilia Boulin y Huguito Victoria', 2615158770, 'Inés Victoria', 2147483647, 'Feliz Cumple Inés!!! ', '2020-10-09', 'Araoz 700, casa 16. Barrio Rincón de Araoz ', 'Lujan - Mayor Drummond', 'Barrio Rincón de Aráoz. Familia José Victoria', 'Picada Fellini (Clásica)', '4', '-Cerveza Golden-Cerveza Scottish Ale', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1610.00', '996.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-09 16:47:59'),
(20, 'con_envio', 'Julieta Rios', 542615799837, 'Diana Gasull', 2147483647, 'Feliz cumple dianita con cariño amigas de la facu', '2020-10-09', 'B° dalvian M2 c23', 'Capital - Ciudad Capital', 'Familia castillo puerta 1', 'Picada Fellini (Clásica)', '4', 'Cerveza IPA', 'Duela', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1830.00', '1316.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-09 18:54:13'),
(22, 'con_reriro', 'Emiliano Candia', 2615989283, '', 0, 'Muy feliz dia de la madre! Te amamos mucho. Emiliano', '2020-10-18', '', '', '', 'Picada Picasso (Premium)', '4', '', 'MDF', '', '', '', 'de 17 a 20', 'Efectivo', '1310.00', '1024.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-09 19:50:43'),
(23, 'con_reriro', 'Emiliano Candia ', 2615989283, '', 0, 'Muy feliz día de la madre!!! Te amamos mucho! Emiliano', '2020-10-18', '', '', '', 'Picada Picasso (Premium)', '4', '', 'MDF', '', '', '', 'de 10 a 13', 'Efectivo', '1310.00', '1024.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-09 19:55:26'),
(24, 'con_reriro', 'Carolina Rivero', 2616547196, '', 0, '', '2020-10-09', '', '', '', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', 'de 17 a 20', 'Efectivo', '950.00', '736.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-09 22:40:17'),
(25, 'con_envio', 'Diego', 2616685585, 'Emiliano', 2147483647, 'gracias por todo de parte de lila', '2020-10-10', 'Rondeau 366 ', 'Capital - Ciudad Capital', '', 'Picada Fellini (Clásica)', '4', '-Cerveza Kolsh', 'MDF', '', '', '', '18:00 - 20:00', 'Efectivo', '1330.00', '916.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-10 00:09:16'),
(26, 'con_envio', 'María Laura Gobbi ', 2612449113, 'Tomás García ', 2147483647, 'Feliz cumple gordii!! Te amo con todo mi corazón!! ', '2020-10-10', 'Sarmiento 664-depto 1', 'Capital - Ciudad Capital', '', 'Picada Fellini (Clásica)', '8', '', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '2020.00', '1652.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-10 14:28:01'),
(27, 'con_reriro', 'Andres Villarruel', 2616665342, '', 0, 'Feliz cumple luquitas, familia Castilla, Houri y Villarruel', '2020-10-10', '', '', '', 'Picada Fellini (Clásica)', '4', '-Cerveza Golden-Cerveza Kolsh-Mix de Chocolates', 'Duela', '', '', '', 'de 17 a 20', 'Mercado Pago', '2320.00', '1136.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-10 21:19:37'),
(29, 'con_envio', 'Mariana Pucciarelli ', 2615165330, 'Benjamin Bauco', 2616915465, 'Feliz cumpleaños \r\nTe amamos!\r\nAlfonsina, Rocko y Gorda', '2020-10-14', 'Virgen de Lujan 452', 'Guaymallen - Villa Nueva', 'Entre Rioja y Bejarano', 'Picada Picasso (Premium)', '4', '', 'Duela', '', '', '', '18:00 - 20:00', 'Mercado Pago', '2000.00', '1644.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-11 21:49:57'),
(30, 'con_envio', 'Paula Garrido', 2613017860, 'Paulo Llambias', 2615322695, 'Muy feliz cumpleaños querido Paulo! Que seas muy feliz, jefe ❤\r\nTe queremos mucho.\r\nCon cariño,\r\nPau, Rama, Milu y Agus', '2020-10-13', 'Alpatacal 1532. Depto 3. Sexta sección ', 'Capital - Ciudad Capital', '', 'Picada Picasso (Premium)', '4', '-Cerveza Porter-Cerveza IPA-Cerveza Scottish Ale', 'Duela', '', '', '', '10:30 - 13:30', 'Mercado Pago', '2590.00', '1604.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-12 00:03:38'),
(31, 'con_envio', 'Erica Gomez', 2613376863, 'Erica Gomez', 2613376863, 'Mi cumple', '2020-10-13', 'Barrio 25 de setiembre o Kolton MBC22', 'Las Heras - Ciudad de Las Heras', 'Belgrano 1153', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '14:00 - 17:00', 'Efectivo', '1170.00', '956.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-13 00:20:59'),
(32, 'con_envio', 'Ana Carla Cascone', 2615874808, 'Aida Videla', 2616967562, 'Feliz día a la mejor!!! Te amamos con el alma!!! El bloque ❤️', '2020-10-17', 'Videla Correas 80', 'Capital - Ciudad Capital', 'Dos casas antes de llegar a 9 de julio. No hay timbre', 'Fellini Dia de la Madre', '4', 'Cerveza Cerveza Golden - Mix de Chocolates', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1800.00', '1440.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-13 15:21:35'),
(34, 'con_envio', 'María Cristina Rodríguez', 1140826967, 'Agustina Dinamarca', 2616997814, '¡Muy feliz cumple, Agus! ¡Te quiero muchísimo! \r\nTe mando un abrazo enorme, \r\nCris', '2020-10-16', 'La Paz 3243', 'Godoy Cruz - Ciudad de Godoy Cruz', '', 'Picada Fellini (Clásica)', '5', '-Cerveza Porter-Cerveza IPA', 'MDF', '', '', '', '14:00 - 17:00', 'Mercado Pago', '1760.00', '1100.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-13 15:22:54'),
(36, 'con_envio', 'poblete edgar raul', 2644522461, 'Patricia Pereyra', 2616842701, 'Feliz día de la madre..Familia Poblete . San Juan', '2020-10-18', 'Av España 934 Piso 2 Dpto 2', 'Capital - Ciudad Capital', 'entre montevideo y rivadavia. frente a un kiosco. tocar portero electrico', 'Fellini Dia de la Madre', '4', 'Cerveza Cerveza Cream - Mix de Chocolates', 'MDF', '', '', '', '10:30 - 13:30', 'Mercado Pago', '1800.00', '1440.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-13 15:30:32'),
(44, 'con_envio', 'Romina Sales y Javier Orbiscay', 2615153651, 'Santiago Soria', 2615137070, 'Para nuestro sobrino preferido. Feliz cumple! te queremos!', '2020-10-17', 'Raquel Yanelli s/n, Barrio Village, MC C19', 'Las Heras - Ciudad de Las Heras', 'Manzana C, casa 19. Envío punto por wathsapp', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '10:30 - 13:30', 'Mercado Pago', '1170.00', '956.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-13 16:29:32'),
(45, 'con_envio', 'Romina Sales y Javier Orbiscay', 2615153651, 'Pedro Orbiscay', 2615170826, 'Feliz cumple Pedrito, te queremos muchísimo!', '2020-10-24', 'Raquel Yanelli s/n, Barrio Village, MC C19', 'Las Heras - Ciudad de Las Heras', 'Manzana C, casa 19. Envío punto por wathsapp', 'Picada Fellini (Clásica)', '4', '-Cerveza Cream-Cerveza Golden', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1570.00', '1080.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-20 01:53:34'),
(46, 'con_envio', 'Veronica García', 2616686460, 'Matías Pestana/ soledad Giménez', 2615501372, 'Gracias tíos! , con cariño Felipe', '2020-10-14', 'Francisco Alvarez 65 PB depto 2', 'Capital - Ciudad Capital', '', 'Picada Fellini (Clásica)', '4', '-Cerveza Porter', 'MDF', '', '', '', '10:30 - 13:30', 'Mercado Pago', '1330.00', '916.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-13 18:06:15'),
(47, 'con_envio', 'Nicolás Pérez Maestri', 2612156013, 'Sandra Maestri', 2615786051, 'Felíz día, Mamá! Te amo! Disfrutalo..', '2020-10-18', 'Barrio Villa Catalina, Casa 25 Oeste', 'Las Heras - El Challao', 'Al lado del supermercado Átomo que se encuentra frente al Parque de la Familia, subiendo por Regalado Olguín, apenas pasando la rotonda se ingresa a la calle lateral que da al barrio. Sino buscar en google maps \"Villa Catalina\" .', 'Fellini Dia de la Madre', '4', 'Cerveza Cerveza Cream - Mix de Chocolates', 'MDF', '', '', '', '18:00 - 20:00', 'Efectivo', '1800.00', '1440.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-13 22:54:50'),
(48, 'con_envio', 'Agustín Pérez', 2616014633, 'Agustín Pérez', 2616014633, '', '2020-10-16', 'Tartagal 3038, Barrio Judicial', 'Godoy Cruz - Ciudad de Godoy Cruz', 'Rejas negras, casa amarilla de 2 pisos', 'Picada Picasso (Premium)', '4', '-Chipa Congelado (250 gr)', 'MDF', '', '', '', '10:30 - 13:30', 'Efectivo', '1740.00', '1204.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-13 23:23:42'),
(50, 'con_reriro', 'Gabriela Cocco', 2614700722, '', 0, 'Feliz día de la Madre', '2020-10-16', '', '', '', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', 'de 17 a 20', 'Mercado Pago', '950.00', '736.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-14 13:42:33'),
(51, 'con_envio', 'Estefanía Varela ', 2616907285, 'Janina Ortiz ', 2616907285, 'Muy Feliz cumple Jani. !!!', '2020-10-14', 'San Luis norte 1054 departamento 2', 'Las Heras - El Zapallar', '', 'Picada Fellini (Clásica)', '4', '', 'Duela', '', '', '', '14:00 - 17:00', 'Mercado Pago', '1640.00', '1356.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-14 13:51:31'),
(52, 'con_envio', 'Alejandra Lucero Velázquez', 2616984727, 'Magui moyano', 2612482515, 'Reina abuela te queremos mucho y fuerte...\r\nLas 11 restantes', '2020-10-14', 'calle José María Gutiérrez 3020', 'Guaymallen - Las Cañas', 'Callejón con portón negro justo antes de calle Terulay', 'MDF Pican 4/ Comen 2 ', '', '', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '180.00', '180.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-14 14:57:59'),
(54, 'con_envio', 'Carla Garcia Alonso', 3412604031, 'Emilio Garcia Alonso', 2614609751, 'Pa!!! Feliz cumple!!!! Te amamos infinitamente!!! Debra, Carla, Paula y Ramiro, pasala hermoso en tu día y disfrutala!!!!!', '2020-10-15', 'Adolfo Calle 2940. Entre Molina y Estrada. ', 'Guaymallen - Villa Nueva', 'No funciona el portero, avisar cuando esten abajo ', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '14:00 - 17:00', 'Mercado Pago', '1170.00', '760.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-15 19:41:42'),
(55, 'con_envio', 'Josefina Brusadin', 2616136766, 'Raquelita Gil', 261, 'Feliz cumple Raquelin! Te queremos! Tus amiguitas', '2020-10-15', 'Almirante Brown 3415', 'Lujan - Vistalba', 'Frente a una barrera amarilla y negra. Portón de madera', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1250.00', '1036.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-14 20:38:51'),
(56, 'con_envio', 'María carolina gadea', 2616706895, 'Agustina navazo ', 2615598571, 'Agus¡¡ felicitaciones por esta nueva etapa que comenzas! Que sean muy felices. Mili, agus, sofi, pili, mica, vero y caro', '2020-10-16', 'San Martín 1710, barrio jardín miraflores, manzana D casa 8', 'Lujan - Carrodilla', ' barrio jardín miraflores familia navazo', 'Picada Fellini (Clásica)', '4', '-Cerveza Golden', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1370.00', '956.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-15 15:50:53'),
(57, 'con_reriro', 'Andres Villarruel', 2616665342, '', 0, '', '2020-10-15', '', '', '', 'Picada Fellini (Clásica)', '4', '-Cerveza Golden-Cerveza Porter-Mix de Chocolates', 'Duela', '', '', '', 'de 17 a 20', 'Mercado Pago', '2320.00', '1136.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-15 17:28:13'),
(58, 'con_envio', 'Guttilla Nicolás', 2614662911, 'Axel Miculi ', 2616641717, 'Te queremos Yeloco, gracias por tu amistad!!\r\nCami Nico Estefania Cristian \r\nGuada Cande Geri Santi \r\nGime Negro Romi Mati ', '2020-10-22', 'Videla Aranda 1351, cruz de piedra', 'Maipu- Cruz de Piedra', 'Cada color bordo con rejas negras ', 'Picada Fellini (Clásica)', '4', '-Cerveza Scottish Ale', 'MDF', '', '', '', '10:30 - 13:30', 'Mercado Pago', '1490.00', '1076.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-20 01:51:39'),
(59, 'con_envio', 'Santiago Baravalle ', 2613051862, 'Santiago Baravalle ', 2613051862, 'Feliz día Mamá!', '2020-10-18', 'Juan José paso 50, casa 6', 'Lujan - La Puntilla', 'Barrio privado EL HUERTO', 'Picada Picasso (Premium)', '5', '-Chipa Congelado (250 gr)', 'MDF', '', '', '', '10:30 - 13:30', 'Efectivo', '2110.00', '1540.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-15 18:33:00'),
(60, 'con_envio', 'Sosa G. Carolina', 2616861408, 'Sosa Carolina', 2616861408, 'FELIZ DIA MAMÁ', '2020-10-17', 'LOS ESCRITORES M:L - C:30 B° Antártida Argentina II', 'Maipu- Ciudad de MaipU', 'Entre calle Mallea y Cortaza -Casa con 2 portones negros', 'Picada Picasso (Premium)', '4', '', 'Duela Unida', '', '', '', '18:00 - 20:00', 'Efectivo', '2100.00', '1724.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-15 19:09:53'),
(61, 'con_envio', 'Agustin Yanzón', 2616609278, 'Adriana Scalambro', 2616578269, 'Feliz día a una increíble mamá. Te queremos mucho!\r\nSanti y Agus.', '2020-10-18', 'Barrio Parque Norte Manzana E casa 27', 'Las Heras - Ciudad de Las Heras', 'La calle no tiene nombre, queda a la vuelta de la verdulería \"Armando\", entre calles 17 de octubre y J J Valle', 'Fellini Dia de la Madre', '4', 'Cerveza Cerveza Scotish Ale - Mix de Chocolates', 'MDF', '', '', '', '10:30 - 13:30', 'Mercado Pago', '1800.00', '1440.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-15 19:04:21'),
(62, 'con_reriro', 'Marcela pereyra', 2614724340, '', 0, 'Leandro: Muchas gracias por atendernos con cariño y responsabilidad!!!\r\nRosi y Marcelo Pereyra!!!', '2020-10-16', '', '', '', 'Picada Picasso (Premium)', '4', '', 'Duela', '', '', '', 'de 17 a 20', 'Mercado Pago', '1780.00', '1424.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-15 20:07:40'),
(64, 'con_envio', 'Rávida Gabriela', 2615385271, 'Edith Brú', 2614852552, 'Muy feliz cumple! Te queremos Gaby y Nahue', '2020-10-16', 'Lavalle 941 ', 'Godoy Cruz - Ciudad de Godoy Cruz', 'En el portero del portón negro', 'Picada Fellini (Clásica)', '6', '', 'MDF', '', '', '', '18:00 - 20:00', 'Efectivo', '1560.00', '1284.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-15 21:35:53'),
(69, 'con_envio', 'Laura Fernandez', 2615098163, 'Laura Fernandez', 2615098163, 'Feliz día mamá!!', '2020-10-18', 'Cadetes Chilenos 1006', 'Guaymallen - Dorrego', 'Esquina Beruti', 'Fellini Dia de la Madre', '4', 'Cerveza Cerveza IPA - Mix de Chocolates', 'MDF', '', '', '', '10:30 - 13:30', 'Efectivo', '1800.00', '1440.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-16 00:38:30'),
(71, 'con_envio', 'Jonatan oropel ', 2616685099, 'Jonatan oropel', 2616685099, 'Te queremos mucho abuelita', '2020-10-18', '', 'Capital - Ciudad Capital', 'Es en el club de la milanesa aristides 405', 'Fellini Dia de la Madre', '4', 'Cerveza Cerveza Golden - Mix de Chocolates', 'MDF', '', '', '', '14:00 - 17:00', 'Efectivo', '1800.00', '1440.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-16 17:46:05'),
(72, 'con_envio', 'Matt, Emma y Felipe ZAPIOLA ', 5491165618737, 'Cristina y Armando Alonso', 2615091152, 'Feliz día de la MADRE querida Abuela💛\r\nFeliz cumple Abuelo Armando. 💛\r\nLos queremos con el alma y los abrazamos  a la distancia. ', '2020-10-18', 'Juan de Dios Videla 746', 'Capital - Ciudad Capital', 'Entre Paso de Los Andes y Huarpes', 'Picada Picasso (Premium)', '4', '-Mix de ChocolatesMix de ChocolatesChipa Congelado (250 gr)', 'Duela Unida', '', '', '', 'de 10:30 - 13:30', 'Mercado Pago', '2560.00', '1904.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-16 22:47:32'),
(73, 'con_envio', 'Mariana Pezzutti ', 2616582010, 'Delia pereyra ', 2613359678, 'Feliz dia te amamos mamita\r\nMarce Cris July San Rami Gaspi Luca y Mary', '2020-10-18', 'Rivadavia 480 2do piso 2', 'Capital - Ciudad Capital', 'Frente plaza independencia edificio kolton', 'Picada Oliverio(Vegetariana)', '4', '-Cerveza Porter', 'MDF', '', '', '', '10:30 - 13:30', 'Mercado Pago', '1150.00', '772.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-16 22:47:23'),
(74, 'con_envio', 'M Lucía Llanos', 2613839975, 'Maria Elena Losada ', 261153839975, 'Para una súper mamu de Joaco, Marti, Roma, Rambo, Papu, Xavi, Sira y Lü ', '2020-10-17', 'Cochabamba 2973 casa 9. Barrio la Candelaria', 'Guaymallen - Las Cañas', 'Es una calle de tierra con varios barrios cerrado es el último a mano derecha', 'Picada Fellini (Clásica)', '4', '-Cerveza Golden-Vino Pacto Cabernet Franc', 'Duela Unida', '', '', '', '18:00 - 20:00', 'Mercado Pago', '2600.00', '1396.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-17 01:34:27'),
(76, 'con_envio', 'Elizabeth Renna', 2616512091, 'Ibel Bermudez', 2616777806, 'Feliz cumpleaños ahijada querida!!! Besos Josefina', '2020-10-19', 'Av. España 3240 ciudad capital', 'Capital - Ciudad Capital', 'Casa blanca con porton doble madera.', 'Picada Fellini (Clásica)', '6', '-Cerveza Porter', 'MDF', '', '', '', '18:00 - 20:00', 'Efectivo', '1760.00', '1284.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-17 14:25:00'),
(77, 'con_envio', 'Lety Peral', 2615448330, 'Claudia Peral', 2615916482, 'Feliz día de la madre Perala de nuestro corazón! Te queremos y deseamos un bello día.', '2020-10-17', 'Azzoni 11', 'Guaymallen - Villa Nueva', 'Entrando al barrio el portal 11 es la primera calle a mano derecha y en esa calle la tercera casa del lado izquierdo ', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1170.00', '956.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-17 15:11:47'),
(78, 'con_envio', 'Marcela Orbiscay', 542615137070, 'Maria Bertilde Riva', 542614711336, 'Feliz día de la Madre!! Un beso grande. Rubén, Marcela, Santi y Mili', '2017-10-20', 'Chaco 1743', 'Las Heras - Ciudad de Las Heras', 'Calle Chaco entre Pucará y Acceso Norte', 'Picada Fellini (Clásica)', '4', '-Mix de Chocolates', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1670.00', '956.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-17 16:03:02'),
(79, 'con_envio', 'Liliana Suarez', 2616009328, 'Fernando Gonzalez', 2616364782, 'Feliz cumple Negrito hermoso. Te amo , mami', '2020-10-24', 'Los Alamos 1793 dpto 5', 'Godoy Cruz - Ciudad de Godoy Cruz', 'Esquina Honorio barraquero', 'Picada Fellini (Clásica)', '4', '', 'Duela Unida', '', '', '', '10:30 - 13:30', 'Mercado Pago', '1700.00', '1396.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-19 15:54:38'),
(80, 'con_envio', 'Ana Maria Caride', 2324557135, 'Clara Caride', 11, 'Lejos pero siempre cerquita!! De parte de tus dos hermanitas preferidas ❤️', '2020-10-19', 'Av Peru 958', 'Capital - Ciudad Capital', 'Rejas negras residencia Atuel', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1130.00', '916.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-19 15:54:48'),
(81, 'con_envio', 'Lucia Piton ', 2616578635, 'Mariana Echeverria ', 2616516101, 'Feliz cumple Mariana y Richard!!!! Con cariño los Sospechosos Tieppos', '2020-10-20', 'Manzana C casa 25 Barrio Nueva Ciudad', 'Maipu- Ciudad de MaipU', '', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '10:30 - 13:30', 'Mercado Pago', '1170.00', '956.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-19 16:23:16'),
(82, 'con_reriro', 'Gisela Aliquo', 2615414418, '', 0, 'Para la hermosa mesa chica! ', '2020-10-19', '', '', '', 'Picada Fellini (Clásica)', '6', '', 'MDF', '', '', '', 'de 17 a 20', 'Efectivo', '1380.00', '1104.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-19 15:38:25'),
(83, 'con_reriro', 'Priscila palazzo ', 2612539850, '', 0, 'Feliz cumpleaños! Te queremos mucho \r\nLauti y Pri ', '2020-10-22', '', '', '', 'Picada Fellini (Clásica)', '5', '', 'MDF', '', '', '', 'de 17 a 20', 'Efectivo', '1150.00', '920.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-19 18:51:53'),
(84, 'con_envio', 'Heliana Rodriguez', 2615729595, 'Leopoldo Rodriguez', 2616607541, 'Feliz cumple Leo!!!\r\nTe queremos!!!\r\nFlia Rodríguez Gatta', '2020-10-24', 'Terruños de Araoz 3, lote C 9. Vieytes 3300Flia. Rodríguez De Paolo', 'Lujan - Mayor Drummond', 'Dejar en la Guardia', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '18:00 - 20:00', 'Transferencia', '1210.00', '760.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-20 12:34:16'),
(85, 'con_envio', 'Laura Perez', 2616348254, 'Roberto Perez ', 2616741597, 'FELIZ CUMPLEAÑOS ROBER!! TE QUEREMOS MUCHO !! Disfrutalo ! \r\nde Clarita, Lau y Fer ! ', '2020-10-21', 'silvano rodriguez 3341 piso 2 dpto 8 Guaymallen ', 'Guaymallen - Villa Nueva', 'ES UN COMPLEJO DE DPTOS PERO AVISAR POR WSP ANTES DE LLEGAR PARA SALIR A RECIBIR. ', 'Picada Fellini (Clásica)', '4', '-Cerveza Golden', 'MDF', '', '', '', '14:00 - 17:00', 'Mercado Pago', '1370.00', '956.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-20 12:34:01'),
(86, 'con_envio', 'Martin irañeta', 2615354313, 'Cristina Rossellot', 2615169203, 'Feliz cumpleaños teacher! Espero que disfrutes esta gran picada! Martin y Mario', '2020-10-20', 'Urquiza 105, barrio las Magnolias ,casa 24', 'Guaymallen - Villa Nueva', 'Callejon con porton electrico. Al final del callejon, a mano izquierda', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '10:30 - 13:30', 'Mercado Pago', '1170.00', '956.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-20 02:34:39'),
(87, 'con_envio', 'Florentina Brodsky', 2615132233, 'Pablo Lipstein ', 2612537623, 'Felices 4 años y medio mansito! Para festejar juntos. Te amo mucho!', '2020-10-23', 'Pueyrredon 2560', 'Lujan - Chacras de Coria', 'Porton negro con cartel: ALTO PUEYRREDON. ', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1170.00', '956.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-20 16:11:47'),
(88, 'con_envio', 'Agustina tornese ', 2612075637, 'Valentina Giovannini ', 2615564555, 'Feliz cumple Valen ! Te queremos mucho! Agus, Luly y Maca ', '2020-10-22', 'Pedro Molina 367, Piso 3, departamento 15', 'Capital - Ciudad Capital', '', 'Picada Fellini (Clásica)', '4', '-Cerveza Golden', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1330.00', '940.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-20 16:12:27'),
(89, 'con_envio', 'BRENDA LUCESOLI', 2616415093, 'GERARDO STELLA', 2612513102, '¡Feliz cumple Geri! Con Cariño. Equipo Kairox!', '2020-10-20', 'Belgrano 1171 depto 3', 'Godoy Cruz - Ciudad de Godoy Cruz', '', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1130.00', '940.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-20 23:08:18'),
(90, 'con_envio', 'BRENDA LUCESOLI', 2616415093, 'ABEL YAIR ABAD', 2616798477, '¡Feliz Cumple Yair! Con cariño. Equipo Kairox!', '2020-10-20', 'Carlos Márquez 295,', 'Guaymallen - Dorrego', '', 'Picada Fellini (Clásica)', '4', '', 'Duela', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1630.00', '1340.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-20 23:08:09'),
(91, 'con_envio', 'Camila Ontivero', 2615340369, 'Leandro Lamagrande', 2615048134, 'Con mucho amor de Valentino y Camila . Feliz cumple! ', '2020-10-24', 'Rafael obligado. B° xv agua y energía. Manzana A casa 15', 'Maipu- Luzuriaga', 'Es en la calle que topa en 9 de julio. Que se encuentra aquatica', 'Picada Picasso (Premium)', '4', '-Mix de Chocolates', 'Duela', '', '', '', 'de 18:00 - 20:00', 'Efectivo', '2520.00', '1840.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-20 23:30:53'),
(92, 'con_envio', 'Sergio MEDINA LUFFI', 261, 'Marcela RANEA', 261, 'Eternamente agradecido a una Flia que constantemente me ha dado tanto cariño y comprensión; pero fundamentalmente por el apoyo en lo legal. FLIA MEDINA...', '2020-10-21', 'Blanco Encalada 688, Alto Dorrego.', 'Guaymallen - Dorrego', 'Esquina, casa de ladrillo visto, portón amplio de madera.', 'Picada Picasso (Premium)', '8', '-Cerveza Cream-Cerveza Golden-Cerveza Porter-Vino Pacto Malbec', 'MDF', '', '', '', '18:00 - 20:00', 'Efectivo', '3860.00', '2244.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-20 20:23:45'),
(93, 'con_envio', 'Macarena Villalobos', 2616992915, 'Carina Azpilcueta', 2615382953, 'Tia querida feliz cumpleaños, espero que pases un día hermoso junto a tu hermosa familia! Gracias por estar siempre! Te quiero mucho! Maca', '2020-10-22', 'General Alvear 851, entre Derqui y 20 de Junio.', 'Godoy Cruz - Ciudad de Godoy Cruz', 'Son dos puertas de pasillo, la puerta es la de la derecha tiene el portero a la izquierda.', 'Picada Fellini (Clásica)', '4', '-Cerveza Cream', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1330.00', '940.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-21 15:37:58'),
(94, 'con_envio', 'Juan Dávila Villanueva', 2614718388, 'Roberto Dávila', 2613648227, 'Feliz cumple Chino !!!!!\r\nJuan y Agus', '2020-10-22', 'Orfila 1676, Mendoza', 'Guaymallen - San Francisco del Monte', 'Casa beige', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1130.00', '940.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-22 00:24:13'),
(95, 'con_reriro', 'Leonardo Bagatolli', 2613059571, '', 0, 'Feliz cumple Fer!', '2020-10-24', '', '', '', 'Picada Fellini (Clásica)', '4', '', 'Duela', '', '', '', 'de 17 a 20', 'Efectivo', '1450.00', '1160.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-22 12:30:14'),
(96, 'con_reriro', 'Gonzalo Descotte', 2612151369, '', 0, '', '2020-10-22', '', '', '', 'Picada Fellini (Clásica)', '4', '-Cerveza Golden-Cerveza Porter-Cerveza Scottish Ale', 'MDF', '', '', '', 'de 17 a 20', 'Mercado Pago', '1550.00', '760.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-22 17:56:16'),
(97, 'con_envio', 'Sosa G. Carolina', 2616861408, 'Luvina Fabrega', 2616663949, 'FELIZ CUMPLE AMIGA!!! Caro - Cari - Romi', '2020-10-24', 'B° Lagos del Torreon M:C C:19 Luzuriagas Maipú', 'Maipu- Luzuriaga', 'La Casa está frente a la Rotonda  ', 'Picada Picasso (Premium)', '4', '', 'MDF', '', '', '', '18:00 - 20:00', 'Transferencia', '1520.00', '1040.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-23 14:10:30'),
(98, 'con_envio', 'Claudia Moreno', 2612576512, 'Ignacio Bittar', 261, 'Feliz cumple Nacho!! Que pases un hermoso dia!!! Te queremos!!\r\nInfecto y control de infecciones❤️', '2020-10-23', 'Presidentes alvear 207 Planta baja', 'Godoy Cruz - Ciudad de Godoy Cruz', 'Presidente alvear 207', 'Picada Fellini (Clásica)', '4', '-Chipa Congelado (250 gr)-Cerveza Kolsh-Cerveza Porter-Mix de Chocolates', 'MDF', '', '', '', '14:00 - 17:00', 'Mercado Pago', '2280.00', '940.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-23 14:41:14'),
(99, 'con_reriro', 'Johana Morales ', 2617019580, '', 0, 'Felices 30! Que sea el comienzo de algo grande! Te Amamos!!', '2020-11-02', '', '', '', 'Picada Fellini (Clásica)', '4', '', 'Duela', '', '', '', 'de 17 a 20', 'Mercado Pago', '1450.00', '1160.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-24 14:26:57'),
(100, 'con_envio', 'Natalia Riveros ', 2615444826, 'Mariela ', 261, 'Que Empieces tu día de la mejor manera! Te queremos Mari! ', '2020-10-24', 'Barrio Parque Urquiza M: E C: 40.  Gllen', 'Guaymallen - San Francisco del Monte', '', 'Picada Fellini (Clásica)', '4', '-Cerveza Golden', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1330.00', '940.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-26 11:32:47'),
(101, 'con_envio', 'Graciela Quesda', 2616659151, 'Graciela quesada ', 2616659151, '', '2020-10-24', 'Benielli 2136 (entre moldes y parana)', 'Capital - Ciudad Capital', '6ta sección ', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1360.00', '944.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-26 11:35:48'),
(102, 'con_envio', 'Graciela quesada', 2616659151, 'Graciela quesada ', 2616659151, '', '2020-10-24', 'Benielli 2136 (entre moldes y parana)', 'Capital - Ciudad Capital', '6ta sección ', 'Picada Fellini (Clásica)', '4', '', 'Duela', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1630.00', '1160.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-26 11:35:40'),
(104, 'con_envio', 'Vanesa Garcia', 2615249773, 'Adriana Copia', 2615350310, 'Felicidades Muñeca!\r\nTe Queremos!! Los García y los Vié', '2020-10-26', 'Huarpes 1848 Barrio San Javier', 'Guaymallen - Capilla del Rosario', 'Enfrente de la plaza, rejas negras', 'Picada Fellini (Clásica)', '4', '-Cerveza Golden-Cerveza Porter', 'Duela', '', '', '', '10:30 - 13:30', 'Mercado Pago', '2070.00', '1480.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-28 16:03:08'),
(105, 'con_envio', 'Mariana Sinigalia', 2616861829, 'Juan pablo di pietro', 2612077901, 'Felices 3 meses mi amor', '2020-10-26', 'Almirante brown 1424 depto 11', 'Godoy Cruz - Ciudad de Godoy Cruz', 'Puede ser horario de entrega cerca de las  20 hs????', 'Picada Picasso (Premium)', '4', '-Cerveza Kolsh', 'MDF', '', '', '', '18:00 - 20:00', 'Transferencia', '1680.00', '1200.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-26 15:25:58'),
(106, 'con_envio', 'Sosa G. Carolina', 2616861408, 'Sosa Osvaldo', 2615338341, 'FELIZ CUMPLE, HERMANIN!!!', '2020-10-28', 'Colón 1705 San José Gllen', 'Guaymallen - San Jose', 'Entre calle Correa Saa y Gdero. De San Martín, tercera casa de ladrillo visto, al lado de una librería.', 'Picada Picasso (Premium)', '4', '-Cerveza Golden-Cerveza Scottish Ale', 'Duela Unida', '', '', '', '18:00 - 20:00', 'Transferencia', '2480.00', '1840.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-27 00:04:50'),
(107, 'con_envio', 'Moyano Aida', 2612165007, 'Maru Marengo', 261, 'Muy feliz cumple Aida y Jorge! ', '2020-10-28', '-32.9561080, -68.7527350 (coordenadas) Urquiza 1890 coquimbito', 'Maipu- Coquimbito', 'Es un callejón a lado de la maderera. nro de referencia de quién está en la vivienda x las dudas 2612164975. Entre la maderera y la casa rosada hay un callejón. Urquiza 1890', 'Picada Fellini (Clásica)', '4', '-Cerveza Porter', 'Duela', '', '', '', '10:30 - 13:30', 'Mercado Pago', '1910.00', '1420.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-27 20:06:49'),
(108, 'con_envio', 'Silvina idañez', 2613899828, 'Alejandro furno', 2612183002, 'Feliz cumpleaños al amor de mi vida', '2020-10-27', 'San lorenzo 444. Dpto 42', 'Capital - Ciudad Capital', '', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '14:00 - 17:00', 'Mercado Pago', '1130.00', '940.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-27 20:06:38'),
(109, 'con_envio', 'Laura Gudiño ', 2616578540, 'Hugo Gudiño', 2615530523, 'FELIZ CUMPLE te queremos Mamá, Delfi, Naty y Juan Pablo ', '2020-10-28', 'B°Terruños de Araoz II M:EC:10 ', 'Maipu- Russel', 'Ruta 60 entre Vieytes y Maza ', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', '18:00 - 20:00', 'Transferencia', '1250.00', '760.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-28 15:52:27'),
(110, 'con_envio', 'sofia jury', 2613749659, 'micaela marengo', 2616507412, '¡Feliz Cumple Mica! Te queremos mucho. Tus Improamiguis.', '2020-10-27', 'Calle Rivadavia manzana A lote 6 departamento 2 barrio alto', 'Maipu- Ciudad de MaipU', '', 'Picada Fellini (Clásica)', '4', '-Cerveza Golden', 'MDF', '', '', '', '18:00 - 20:00', 'Mercado Pago', '1370.00', '980.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-28 15:51:41'),
(111, 'con_envio', 'María Sol Mejías', 2615650308, 'María Inés Crisafulli', 2612061040, 'Feliz cumpleaños! Te amamos! Juan, Sol, Agu, Santi y Naza', '2020-10-30', 'Talcahuano 326', 'Lujan - La Puntilla', 'Portón de rejas negras', 'Picada Picasso (Premium)', '4', '-Vino Pacto Malbec', 'Duela Rectangular', '', '', '', '10:30 - 13:30', 'Mercado Pago', '2660.00', '1780.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-28 15:52:07'),
(113, 'con_envio', 'Alejandro Noli', 2616168232, 'Carina Lucero', 2613066470, 'Cari, Feliz Cumple!!! Que se cumplan todos tus proyectos... Te queremos mucho.... Coni, Lucre, Agus, Pato y Ale', '2020-10-29', 'Paso de los Andes 1476, Sexta Sección', 'Capital - Ciudad Capital', '', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', 'de 14:00 - 17:00', 'Mercado Pago', '1130.00', '760.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-28 15:52:14'),
(117, 'con_envio', 'Julia cuitiño ', 2615981522, 'Marcelo cuitiño ', 2615402608, 'Feliz cumple Papi! Te queremos mucho. Juli, Juani, Edu, Ale, Jero, Manu, Valen, Vicen y Pablo ', '2020-10-29', 'Santa María de oro 552', 'Capital - Ciudad Capital', 'Esquina calle Misiones sexta seccion', 'Picada Picasso (Premium)', '4', '', 'MDF', '', '', '', 'de 14:00 - 17:00', 'Mercado Pago', '1480.00', '1040.00', 0, 0, 0, '', 'PARA HACER', 'PAGADO', '2020-10-29 00:11:06'),
(118, 'con_reriro', 'Lucas iarowai', 2615946280, '', 0, 'Feliz cumpleaños amigos! De parte de la flia grzona/iarowai', '2020-10-30', '', '', '', 'Picada Fellini (Clásica)', '4', '', 'MDF', '', '', '', 'de 10 a 13', 'Efectivo', '950.00', '760.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-29 01:54:36'),
(119, 'con_reriro', 'juan', 23456789098765, '', 0, 'manso', '2020-10-31', '', '', '', 'Picada Fellini (Clásica)', '10', 'Chipa Congelado (250 gr) - Cerveza Cream - Chipa Congelado (250 gr)', 'MDF', '', '', '', 'de 10 a 13', 'Mercado Pago', '2780.00', '1864.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-29 13:22:21'),
(120, 'con_envio', 'CHICONI PUCHOL GIULIANA', 2622678322, 'DELLA GASPERA FABIAN ', 2622539180, 'LA DISTANCIA SEPARA CUERPOS NO CORAZONES \r\nFELIZ CUMPLEAÑOS! \r\nOSVAL-GIU- MONI- GUILLE', '2020-10-29', 'Necochea 85. Piso 7 Depto 1', 'Capital - Ciudad Capital', 'Casi 9 de julio, a metros del Banco Macro. Edificio puerta de madera. ', 'Picada Fellini (Clásica)', '4', '-Cerveza Kolsh-Mix de Chocolates', 'MDF', '', '', '', '14:00 - 17:00', 'Mercado Pago', '1830.00', '940.00', 0, 0, 0, '', 'PARA HACER', 'NO PAGADO', '2020-10-29 14:39:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `out_list`
--

CREATE TABLE `out_list` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `invoice` int(11) NOT NULL,
  `proveedor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `out_ars` decimal(11,2) NOT NULL,
  `pagos` decimal(11,2) NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createrd_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `out_list`
--

INSERT INTO `out_list` (`id`, `date`, `invoice`, `proveedor`, `out_ars`, `pagos`, `user`, `createrd_at`) VALUES
(1, '2020-10-08 00:05:44', 0, '', '0.00', '0.00', '', '2020-10-08 00:05:44'),
(2, '2020-10-08 00:06:07', 0, '', '0.00', '0.00', '', '2020-10-08 00:06:07'),
(3, '2020-10-08 19:48:53', 0, '', '0.00', '0.00', '', '2020-10-08 19:48:53'),
(4, '2020-10-08 19:49:14', 0, '', '0.00', '0.00', '', '2020-10-08 19:49:14'),
(5, '2020-10-08 19:49:33', 0, '', '0.00', '0.00', '', '2020-10-08 19:49:33'),
(6, '2020-10-08 22:13:17', 0, '', '0.00', '0.00', '', '2020-10-08 22:13:17'),
(7, '2020-10-08 22:13:46', 0, 'Familia Iannizotto', '0.00', '0.00', '', '2020-10-08 22:13:46'),
(8, '2020-10-08 22:14:10', 0, '', '0.00', '0.00', '', '2020-10-08 22:14:10'),
(9, '2020-10-08 22:14:20', 0, '', '0.00', '0.00', '', '2020-10-08 22:14:20'),
(10, '2020-10-08 22:14:31', 0, '', '0.00', '0.00', '', '2020-10-08 22:14:31'),
(11, '2020-10-08 22:14:42', 0, '', '0.00', '0.00', '', '2020-10-08 22:14:42'),
(12, '2020-10-08 22:14:55', 0, '', '0.00', '0.00', '', '2020-10-08 22:14:55'),
(13, '2020-10-08 22:15:12', 0, '', '0.00', '0.00', '', '2020-10-08 22:15:12'),
(14, '2020-10-08 22:15:21', 0, '', '0.00', '0.00', '', '2020-10-08 22:15:21'),
(15, '2020-10-08 22:15:31', 0, '', '0.00', '0.00', '', '2020-10-08 22:15:31'),
(16, '2020-10-08 22:15:42', 0, '', '0.00', '0.00', '', '2020-10-08 22:15:42'),
(17, '2020-10-08 22:15:48', 0, '', '0.00', '0.00', '', '2020-10-08 22:15:48'),
(18, '2020-10-08 22:15:59', 0, '', '0.00', '0.00', '', '2020-10-08 22:15:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_method`
--

CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL,
  `modo_de_pago` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `payment_method`
--

INSERT INTO `payment_method` (`id`, `modo_de_pago`, `description`, `created_at`, `user`) VALUES
(1, 'Mercado Pago', '', '2020-08-18 15:37:30', ''),
(2, 'Efectivo', '', '2020-08-18 15:37:30', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `picadas`
--

CREATE TABLE `picadas` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `in_ars` decimal(11,2) NOT NULL,
  `out_ars` decimal(11,2) NOT NULL,
  `pagina` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `picadas`
--

INSERT INTO `picadas` (`id`, `title`, `description`, `in_ars`, `out_ars`, `pagina`, `created_at`, `type`, `user`) VALUES
(2, 'MDF Pican 4/ Comen 2 ', 'Picada Fellini', '950.00', '760.00', 'si', '2020-10-06 14:40:58', 'MDF', ''),
(3, 'MDF Pican 5', 'Picada Fellini', '1180.00', '944.00', 'si', '2020-10-06 14:41:02', 'MDF', ''),
(4, 'MDF Pican 6/ Comen 3 ', 'Picada Fellini', '1410.00', '1128.00', 'si', '2020-10-06 14:41:06', 'MDF', ''),
(6, 'MDF Pican 8/ Comen 4', 'Picada Fellini', '1870.00', '1496.00', 'no', '2020-10-06 14:42:17', 'MDF', ''),
(7, 'MDF Pican 9', 'Picada Fellini', '2100.00', '1680.00', 'no', '2020-10-06 14:41:15', 'MDF', ''),
(8, 'MDF Pican 10/ Comen 5 ', 'Picada Fellini', '2330.00', '1784.00', 'no', '2020-10-06 14:41:19', 'MDF', ''),
(10, 'MDF Pican 12/ Comen 6', 'Picada Fellini', '2790.00', '2232.00', 'no', '2020-10-06 14:41:23', 'MDF', ''),
(11, 'Duela Entera Pican 4/ Comen 2 ', 'Picada Fellini', '1450.00', '1120.00', 'si', '2020-10-06 14:41:28', 'duelas', ''),
(12, 'Duelas rectangular Pican 4/ Comen 2 ', 'Picada Fellini', '1550.00', '1240.00', 'si', '2020-10-27 16:00:19', 'Duela Rectangular', ''),
(17, 'MDF Pican 4/ Comen 2 ', 'Picada Oliverio', '1150.00', '920.00', 'si', '2020-10-06 14:42:01', 'MDF', ''),
(18, 'MDF Pican 7', 'Picada Fellini', '1640.00', '1312.00', 'no', '2020-10-06 14:42:31', 'MDF', ''),
(19, 'MDF Pican 11', 'Picada Fellini', '2560.00', '2048.00', 'no', '2020-10-06 14:42:33', 'MDF', ''),
(20, 'Duela Entera Pican 4/ Comen 2 ', 'Picada Picasso', '1750.00', '1200.00', 'si', '2020-10-06 14:42:39', 'duelas', ''),
(21, 'MDF Pican 4/ Comen 2 ', 'Picada Picasso', '1300.00', '1040.00', 'si', '2020-10-20 13:28:08', 'MDF', ''),
(22, 'MDF pican 5', 'Picada Picasso', '1620.00', '1296.00', 'si', '2020-10-20 13:28:19', 'MDF', ''),
(23, 'MDF Pican 6/ Comen 3 ', 'Picada Picasso', '1940.00', '1552.00', 'si', '2020-10-20 13:28:29', 'MDF', ''),
(24, 'MDF Pican 7', 'Picada Picasso', '2260.00', '1808.00', 'no', '2020-10-20 13:28:51', 'MDF', ''),
(25, 'MDF Pican 8/ Comen 4', 'Picada Picasso', '2580.00', '2604.00', 'no', '2020-10-20 13:29:08', 'MDF', ''),
(26, 'Duelas rectangular Pican 4/ Comen 2 ', 'Picada Picasso', '1900.00', '1520.00', 'si', '2020-10-27 16:00:28', 'Duela Rectangular', ''),
(27, 'MDF pican 5', 'Picada Oliverio', '1435.00', '1148.00', 'si', '2020-10-20 13:30:09', 'MDF', ''),
(28, 'MDF Pican 6/ Comen 3 ', 'Picada Oliverio', '1720.00', '1376.00', 'si', '2020-10-20 13:30:24', 'MDF', ''),
(29, 'Duela Entera Pican 4/ Comen 2 ', 'Picada Oliverio', '1650.00', '1320.00', 'si', '2020-10-20 13:30:43', 'duelas', ''),
(30, 'Duelas rectangular Pican 4/ Comen 2 ', 'Picada Oliverio', '1750.00', '1400.00', 'si', '2020-10-27 16:00:38', 'Duela Rectangular', ''),
(31, 'MDF Agregado Comensal', 'Picada Fellini', '230.00', '184.00', 'si', '2020-10-20 13:31:12', 'MDF', ''),
(32, 'MDF Agregado Comensal', 'Picada Picasso', '320.00', '256.00', 'si', '2020-10-20 13:31:32', 'MDF', ''),
(33, 'MDF Agregado Comensal', 'Picada Oliverio', '285.00', '228.00', 'si', '2020-10-06 14:52:21', 'MDF', ''),
(34, 'Promo Día de la Madre', 'Picada Fellini para 4 + Cerveza + Mix Chocolate + Envió ', '1800.00', '1440.00', '', '2020-10-20 13:32:00', 'MDF', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preparadas`
--

CREATE TABLE `preparadas` (
  `id` int(11) NOT NULL,
  `picada` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tamano` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_preparacion` date NOT NULL,
  `asignacion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `preparadas`
--

INSERT INTO `preparadas` (`id`, `picada`, `tamano`, `tipo`, `fecha_preparacion`, `asignacion`, `user`, `created_at`) VALUES
(4, 'Picada Fellini', 'Pican 4', 'MDF', '2020-10-07', 'NO ASIGNADA', '', '2020-10-07 23:42:16'),
(5, 'Picada Fellini', 'Pican 4', 'MDF', '2020-10-07', 'NO ASIGNADA', '', '2020-10-07 23:42:54'),
(6, 'Picada Fellini', 'Pican 4', 'MDF', '2020-10-07', 'NO ASIGNADA', '', '2020-10-07 23:43:08'),
(7, 'Picada Fellini', 'Pican 4', 'MDF', '2020-10-08', 'NO ASIGNADA', '', '2020-10-08 22:25:44'),
(8, 'Picada Fellini', 'Pican 4', 'MDF', '2020-10-08', 'NO ASIGNADA', '', '2020-10-08 22:25:59'),
(9, 'Picada Fellini', 'Pican 4', 'MDF', '2020-10-08', 'NO ASIGNADA', '', '2020-10-08 22:27:11'),
(10, 'Picada Fellini', 'Pican 4', 'Duela', '2020-10-08', 'NO ASIGNADA', '', '2020-10-08 22:27:28'),
(11, 'Picada Fellini', 'Pican 4', 'Duela', '2020-10-08', 'NO ASIGNADA', '', '2020-10-08 22:27:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preparada_picada`
--

CREATE TABLE `preparada_picada` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `preparada_picada`
--

INSERT INTO `preparada_picada` (`id`, `title`, `foto`, `user`) VALUES
(1, 'Picada Fellini', '', ''),
(2, 'Picada Picasso', '', ''),
(3, 'Oliverio', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id` int(11) NOT NULL,
  `razon_social` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_id` bigint(11) NOT NULL,
  `cta_cte` decimal(11,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id`, `razon_social`, `description`, `tax_id`, `cta_cte`, `created_at`, `user`) VALUES
(1, 'Leonardo Binci', 'Proveedor Vino', 456678, '0.00', '2020-09-07 15:14:53', ''),
(2, 'Familia Iannizotto', 'Proveedor Vino', 7766, '0.00', '2020-09-07 15:14:53', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rango_picada`
--

CREATE TABLE `rango_picada` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_ars` decimal(11,2) NOT NULL,
  `out_ars` decimal(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `rango_picada`
--

INSERT INTO `rango_picada` (`id`, `title`, `in_ars`, `out_ars`) VALUES
(1, 'Picada Fellini (Clásica)', '230.00', '184.00'),
(2, 'Picada Picasso (Premium)', '320.00', '256.00'),
(3, 'Picada Oliverio(Vegetariana)', '285.00', '228.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_pago`
--

CREATE TABLE `status_pago` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `status_pago`
--

INSERT INTO `status_pago` (`id`, `title`, `user`, `created_at`) VALUES
(1, 'PAGADO', '', '2020-09-30 13:24:40'),
(2, 'RECLAMADO', '', '2020-09-30 13:24:40'),
(3, 'NO PAGADO', '', '2020-09-30 13:24:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status_pícadas`
--

CREATE TABLE `status_pícadas` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createrd_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `status_pícadas`
--

INSERT INTO `status_pícadas` (`id`, `title`, `createrd_at`, `user`) VALUES
(1, 'PARA HACER', '2020-08-20 20:13:12', ''),
(2, 'LISTA', '2020-08-20 20:13:12', ''),
(3, 'PARA ENTREGAR', '2020-08-20 20:13:12', ''),
(4, 'ENTREGADA', '2020-08-20 20:13:12', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock`
--

CREATE TABLE `stock` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cuantity` int(11) NOT NULL,
  `in_ars` decimal(11,2) NOT NULL,
  `out_ars` decimal(11,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `stock`
--

INSERT INTO `stock` (`id`, `title`, `description`, `provider`, `cuantity`, `in_ars`, `out_ars`, `created_at`, `user`) VALUES
(1, 'Vino Pacto', 'Vino de Abogado Pelado', 'El Binci', 8, '139.00', '0.00', '2020-09-16 13:47:42', ''),
(2, 'Cerveza Tirada', 'Cerveza con un golpe al piso', 'cerveceria Manuel', 3, '100.00', '0.00', '2020-09-11 01:50:38', ''),
(3, 'Vino Maria Dolores', 'Vino Flia Ianissotto', 'Flia Ianizzotto', 13, '150.00', '120.00', '2020-08-25 16:17:53', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tamano`
--

CREATE TABLE `tamano` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo`
--

CREATE TABLE `tipo` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tipo`
--

INSERT INTO `tipo` (`id`, `title`, `foto`, `user`) VALUES
(1, 'MDF', '', ''),
(2, 'Duela', '', ''),
(3, 'Duela Rectangular', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_picadas`
--

CREATE TABLE `type_picadas` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_ars` decimal(11,2) NOT NULL,
  `comentario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `out_ars` decimal(11,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `type_picadas`
--

INSERT INTO `type_picadas` (`id`, `title`, `in_ars`, `comentario`, `out_ars`, `created_at`) VALUES
(1, 'MDF', '0.00', '', '0.00', '2020-10-07 20:31:15'),
(2, 'Duela', '500.00', '(Incluye el Precio de la Duela)', '400.00', '2020-10-07 15:28:01'),
(3, 'Duela Rectangular', '600.00', '(Incluye el Precio de la Duela)', '480.00', '2020-10-27 15:50:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `celular` bigint(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `last_name` varchar(29) NOT NULL,
  `Created_at` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `empresa` varchar(255) NOT NULL,
  `permiso` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `username`, `photo`, `email`, `pass`, `celular`, `name`, `last_name`, `Created_at`, `empresa`, `permiso`) VALUES
(36, 'tinchomacanudo', '', 'tincho@gmail.com', 'e4f3677c51963c08ff8c83d88079c698', 5492612128105, 'Martin ', 'Cestari', '2020-09-03 18:54:36.795158', 'macanudas', 'super_user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `variables`
--

CREATE TABLE `variables` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `variable` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_ars` decimal(11,2) NOT NULL,
  `out_ars` decimal(11,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `variables`
--

INSERT INTO `variables` (`id`, `title`, `variable`, `in_ars`, `out_ars`, `created_at`, `user`) VALUES
(1, 'km_delivery', 'precio del kilometro delibery', '40.00', '0.00', '2020-08-25 18:26:45', ''),
(2, 'add1', 'Agregado por Picada', '210.00', '150.00', '2020-08-25 16:13:02', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `add2`
--
ALTER TABLE `add2`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `delivery`
--
ALTER TABLE `delivery`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `delivery_hour`
--
ALTER TABLE `delivery_hour`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `general`
--
ALTER TABLE `general`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `out_list`
--
ALTER TABLE `out_list`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `picadas`
--
ALTER TABLE `picadas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `preparadas`
--
ALTER TABLE `preparadas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `preparada_picada`
--
ALTER TABLE `preparada_picada`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rango_picada`
--
ALTER TABLE `rango_picada`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status_pago`
--
ALTER TABLE `status_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status_pícadas`
--
ALTER TABLE `status_pícadas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo`
--
ALTER TABLE `tipo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `type_picadas`
--
ALTER TABLE `type_picadas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indices de la tabla `variables`
--
ALTER TABLE `variables`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `add2`
--
ALTER TABLE `add2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `delivery`
--
ALTER TABLE `delivery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `delivery_hour`
--
ALTER TABLE `delivery_hour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `general`
--
ALTER TABLE `general`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT de la tabla `out_list`
--
ALTER TABLE `out_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `payment_method`
--
ALTER TABLE `payment_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `picadas`
--
ALTER TABLE `picadas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `preparadas`
--
ALTER TABLE `preparadas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `preparada_picada`
--
ALTER TABLE `preparada_picada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `rango_picada`
--
ALTER TABLE `rango_picada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `status_pago`
--
ALTER TABLE `status_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `status_pícadas`
--
ALTER TABLE `status_pícadas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo`
--
ALTER TABLE `tipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `type_picadas`
--
ALTER TABLE `type_picadas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `variables`
--
ALTER TABLE `variables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
